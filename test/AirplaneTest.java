import org.junit.Before;
import play.test.Fixtures;
import play.test.UnitTest;

/**
 * User: matej
 * Date: 10.6.12
 */
public class AirplaneTest extends UnitTest{

    @Before
    public void setup() {
        Fixtures.deleteDatabase();
        Fixtures.loadModels("conf/data.yml");
    }

//    @Test
//    public void testAirplaneFixture(){
//        Airplane airplane=Airplane.find("bySign","OK-KNH").first();
//
//        assertNotNull(airplane);
//        assertEquals("Z-142", airplane.type);
//        assertEquals(6,airplane.Flights.size());
//    }
//
//    @Test
//    public void createAndRetrieveAirplane(){
//        new Aircraft("OK-ABC","Z-326").save();
//        Airplane airplane=Airplane.find("bySign","OK-ABC").first();
//
//        assertNotNull(airplane);
//        assertEquals("Z-326", airplane.type);
//    }
}
