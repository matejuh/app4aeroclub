import models.User;
import org.junit.Before;
import org.junit.Test;
import play.test.Fixtures;
import play.test.UnitTest;

/**
 * User: matej
 * Date: 10.6.12
 */
public class UserTest extends UnitTest{

    @Before
    public void setup() {
        Fixtures.deleteDatabase();
        Fixtures.loadModels("conf/data.yml");
    }

    @Test
    public void testUserFixture(){
        User bob = User.find("byEmail", "bob@gmail.com").first();

        assertNotNull(bob);
        assertEquals("Bob", bob.firstName);

        assertEquals(11,bob.pilotFlights.size());
    }

    @Test
    public void createAndRetrieveUser(){
        new User("john.doe@gmail.com", "John Doe", "secret").save();

        // Retrieve the user with e-mail address bob@gmail.com
        User user = User.find("byEmail", "john.doe@gmail.com").first();

        // Test
        assertNotNull(user);
        assertEquals("John Doe", user.firstName);
    }
}
