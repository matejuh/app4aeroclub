import models.*;
import org.junit.Before;
import org.junit.Test;
import play.test.Fixtures;
import play.test.UnitTest;

/**
 * User: matej
 * Date: 10.6.12
 */
public class FlightTest extends UnitTest {

    @Before
    public void setup() {
        Fixtures.deleteDatabase();
        Fixtures.loadModels("conf/data.yml");
    }

    @Test
    public void testFlightFixture(){
        Flight flight=Flight.find("byPilot.firstName","Bob").first();

        assertNotNull(flight);
        assertEquals("Z-142", flight.airplane.type);
    }

//    @Test
//    public void createAndRetrieveFlight(){
//        Aircraft airplane=new Aircraft("OK-MIG","Z-226").save();
//        User pilot=new User("jonh@gmail.com", "John", "secret").save();
//
////        new AircraftFlight(airplane,pilot).save();
//
//        Flight flight=Flight.find("byPilot.firstName","John").first();
//
//        assertNotNull(flight);
//        assertEquals("Z-226", flight.airplane.type);
//    }
}
