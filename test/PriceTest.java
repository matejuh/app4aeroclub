import models.prices.FlightRelatedPrice;
import org.junit.Before;
import org.junit.Test;
import play.test.Fixtures;
import play.test.UnitTest;

import java.util.ArrayList;
import java.util.List;

/**
 * User: matej
 * Date: 12.6.12
 */
public class PriceTest extends UnitTest {
    @Before
    public void setup() {
        Fixtures.deleteDatabase();
        Fixtures.loadModels("conf/data.yml");
    }

    @Test
    public void testPriceFixture(){
        List<FlightRelatedPrice> prices= FlightRelatedPrice.findAll();
        assertNotNull(prices);
        assertEquals(4,prices.size());
//        assertEquals(200,Prices.get(0).price);
        assertEquals(2,prices.get(0).tags.size());
    }

    @Test
    public void testFindByTag(){
        List <FlightRelatedPrice> prices= FlightRelatedPrice.find("select distinct p from Price p join p.tags as t where t.firstName in ?", "Commercial").fetch();
        assertNotNull(prices);
        assertEquals(1,prices.size());
    }

    @Test
    public void testFindByMultipleTags(){
        List<String>tags=new ArrayList<String>();
        tags.add("Commercial");
        tags.add("Airplane");
        assertEquals(1, FlightRelatedPrice.find(
                "select distinct p from Price p join p.tags as t where t.firstName in (:tags) group by p.price having count(t.id) = :size"
        ).bind("tags", tags).bind("size", tags.toArray().length).fetch().size());
    }

    @Test
    public void testPriceBasic(){
        FlightRelatedPrice price=new FlightRelatedPrice(100).save();
        assertEquals(5, FlightRelatedPrice.findAll().size());
        price.tagItWith("Member").save();
        assertEquals(4, FlightRelatedPrice.findTaggedWith("Member").size());
    }

//    @Test
//    public void testAirplanePrice(){
//        Airplane airplane=Airplane.find("bySign","OK-IFG").first();
//        assertNotNull(airplane);
//        assertEquals(3, airplane.pricesList.size());
//        Logger.info(airplane.pricesList.toString());
//        List<String>tags=new ArrayList<String>();
//        tags.add("Member");
//        tags.add("0");
//        List<Price> Prices=Price.find(
//                "select distinct p from Price p join p.tags as t where p.airplane.sign='OK-IFG' and t.firstName in (:tags) group by p.price having count(t.id) = :size"
//        ).bind("tags", tags).bind("size", tags.toArray().length).fetch();
//        Logger.info(Prices.toString());
//    }

}
