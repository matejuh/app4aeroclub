import models.PaymentType;
import org.junit.Test;
import play.test.UnitTest;

import java.util.List;

/**
 * User: matej
 * Date: 1.7.12
 */
public class PaymentTypeTest extends UnitTest {
    @Test
    public void testCreateAndRetrievePaymentType(){
        new PaymentType("commercial").save();
        new PaymentType("school").save();
        new PaymentType("members").save();
        new PaymentType("students").save();
        List<PaymentType> types=PaymentType.findAll();
        assertEquals(4,types.size());
    }
}
