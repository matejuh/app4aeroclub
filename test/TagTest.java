import models.Tag;
import org.junit.Before;
import org.junit.Test;
import play.test.Fixtures;
import play.test.UnitTest;

/**
 * User: matej
 * Date: 13.6.12
 */
public class TagTest extends UnitTest{
    @Before
    public void setup() {
        Fixtures.deleteDatabase();
        Fixtures.loadModels("conf/data.yml");
    }

    @Test
    public void testTagFixture(){
        Tag tag=Tag.find("byName","Commercial").first();

        assertNotNull(tag);
        assertEquals("Commercial", tag.name);
    }
}
