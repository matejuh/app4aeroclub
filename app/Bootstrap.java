import models.Airplane;
import models.User;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

/**
 * User: matej
 * Date: 10.6.12
 */

@OnApplicationStart
public class Bootstrap extends Job {
    public void doJob() {
        // Check if the database is empty
        if(Airplane.count() == 0) {
            Fixtures.loadModels(
                    "yml/paymentTypes.yml",
                    "yml/winchSettings.yml",
                    "yml/jobSettings.yml",
                    "yml/feeSettings.yml",
                    "yml/instructorSettings.yml",
                    "yml/users.yml",
                    "yml/airplanes.yml",
                    "yml/prices/asw15_prices.yml",
                    "yml/prices/duo_prices.yml",
                    "yml/prices/L23_prices.yml",
                    "yml/prices/L13_prices.yml",
                    "yml/prices/L33_prices.yml",
                    "yml/prices/cirrus_prices.yml",
                    "yml/prices/dyn1_prices.yml",
                    "yml/prices/morane_prices.yml",
                    "yml/prices/z42_prices.yml",
                    "yml/incomes.yml",
                    "yml/flights.yml",
                    "yml/jobs.yml"
//                    "yml/expenditures.yml"
            );
        }
    }
}
