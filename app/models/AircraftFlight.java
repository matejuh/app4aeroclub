package models;

import controllers.payment.CustomPayment;
import controllers.payment.DefaultPayment;
import controllers.payment.PaymentStrategy;
import models.cashflows.CashFlow;
import models.prices.InstructorPrice;
import models.prices.InstructorPricePrice;
import models.prices.TowPrice;
import models.prices.TowPricePrice;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import play.Logger;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.List;

/**
 * User: matej
 * Date: 12.6.12
 */

@Entity
public class AircraftFlight extends Flight{

    @OneToOne(mappedBy = "towFlight",cascade = CascadeType.ALL)
    public GliderFlight towedFlight;

    public boolean isNightFlight=false;

    public boolean isTowFlight=false;

    @Override
    public double getAirplaneRentCost() {
        if(this.isTowFlight){
            TowPricePrice price=((Aircraft)this.airplane).getTowPrice(this.paymentType, this.departureTime);
            return price.towPayment.getCost(this.duration,price);
        }
        return super.getAirplaneRentCost();
    }

    public double getInstructorCost(){
        if(this.isFlightWithInstructor){
            InstructorPricePrice instructorPrice=InstructorPrice.getInstructorPrice(this.paymentType, InstructorPrice.FlightType.aircraft,this.departureTime);
            return (CashFlow.getCost(this.duration, instructorPrice.price)+instructorPrice.startPrice);
        }
        return 0.0;
    }

    public double getStartCost(){
        return 0.0;
    }

    @Override
    public Flight getTowFlight() {
        return this.towedFlight==null? null : this.towedFlight;
    }

    public void propagateTowFields(GliderFlight towedFlight){
            towedFlight.towFlight=this;
            towedFlight.departurePlace=this.departurePlace;
            towedFlight.departureTime=this.departureTime;
            towedFlight.startType= GliderFlight.StartType.tow;
            towedFlight.paymentType=this.paymentType;
    }

    @Override
    public void _save() {
        Logger.debug("saving aircraft flight");
        final PaymentStrategy paymentStrategy;
        if(this.hasDefaultPayment){
            paymentStrategy=new DefaultPayment();
        }
        else{
            paymentStrategy=new CustomPayment();
        }
        try{
            if(this.isTowFlight){
                //paymentStrategy.processPayment(this.towedFlight, this.billsList);
            }
            else{
                paymentStrategy.processPayment(this,this.billsList);
            }
        }
        catch(NoResultException noResultEx){
            Logger.error(noResultEx.getMessage());
        }
        super._save();
    }
}
