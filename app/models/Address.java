package models;

import play.db.jpa.Model;

import javax.persistence.Entity;

/**
 * User: matej
 * Date: 27.10.12
 */
@Entity
public class Address extends Model{
    public String street;

    public String houseNumber;

    public String town;

    public String postalCode;

    public String country;

}
