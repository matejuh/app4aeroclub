package models;

import models.cashflows.CashFlow;
import models.cashflows.FlightBill;
import models.cashflows.InstructorWork;
import models.prices.WinchPrice;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jpa.Model;
import utils.DateTimeInPast;
import utils.DontSerialize;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User: matej
 * Date: 10.6.12
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Flight extends Model {

    @Required
    @ManyToOne
    @DontSerialize
    public User pilot;

    @ManyToOne
    @DontSerialize
    public User secondCrewMember;

    @Required
    public boolean isFlightWithInstructor=false;

    @Required
    @ManyToOne
    public Airplane airplane;

    @Required(message = "Departure time required!")
    @DateTimeInPast
    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy HH:mm")
    public DateTime departureTime;

    public String departurePlace;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy HH:mm")
    public DateTime arrivalTime;

    public String arrivalPlace;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDuration")
    @Required
    @As("HH:mm")
    public Duration duration;

    @Required
    @ManyToOne
    public PaymentType paymentType;

    public String task;

    @OneToMany(mappedBy="flight",cascade = CascadeType.ALL)
    public List<FlightBill> billsList;

    @OneToOne(mappedBy = "instructorFlight",cascade = CascadeType.ALL)
    public InstructorWork instructorBill;

    public boolean hasDefaultPayment=true;

    @Transient
    public Flight towFlight;

    public double getFlightCost() throws NoResultException{
        double airplaneRentCost=this.getAirplaneRentCost();
        double instructorCost=this.getInstructorCost();
        double startCost=this.getStartCost();
        return (airplaneRentCost+instructorCost+startCost);
    }

    public double getAirplaneRentCost() throws NoResultException{
        int pilotYearlyHours=this.pilot.getHoursPerYear(this.departureTime,this.getClass().getSimpleName());
        double airplaneHourRent=this.airplane.getHourPrice(pilotYearlyHours,this.paymentType,this.departureTime);
        return CashFlow.getCost(this.duration, airplaneHourRent);
    }

    public abstract double getInstructorCost();

    public abstract double getStartCost();

    public abstract Flight getTowFlight();


//    public abstract void propagateTowFields();

    @Override
    public String toString() {
        return "Flight{" +
                "pilot=" + pilot +
                ", secondCrewMember=" + secondCrewMember +
                ", isFlightWithInstructor=" + isFlightWithInstructor +
                ", airplane=" + airplane +
                ", departureTime=" + departureTime +
                ", duration=" + duration +
                '}';
    }
}
