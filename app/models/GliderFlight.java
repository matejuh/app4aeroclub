package models;

import controllers.payment.CustomPayment;
import controllers.payment.DefaultPayment;
import controllers.payment.PaymentStrategy;
import models.cashflows.CashFlow;
import models.prices.InstructorPrice;
import models.prices.InstructorPricePrice;
import models.prices.WinchPrice;
import play.Logger;
import play.data.validation.Required;

import javax.persistence.*;

/**
 * User: matej
 * Date: 12.6.12
 */

@Entity
public class GliderFlight extends Flight{

    public static enum StartType {
        winch {
            public double getCost(GliderFlight flight){
                return WinchPrice.getWinchPrice(flight.paymentType, flight.departureTime).price;
            }
        },
        tow{
            public double getCost(GliderFlight flight) throws NoResultException{
                return flight.towFlight.getFlightCost();
            }
        },
        other{
            public double getCost(GliderFlight flight){
                return 0.0;
            }
        };

        public abstract double getCost(GliderFlight flight);
    }

    @Required
    public StartType startType;

    @OneToOne(cascade = CascadeType.ALL)
    public AircraftFlight towFlight;

    public double getInstructorCost(){
        if(isFlightWithInstructor){
            InstructorPricePrice instructorPrice=InstructorPrice.getInstructorPrice(this.paymentType, InstructorPrice.FlightType.glider, this.departureTime);
            return (CashFlow.getCost(this.duration, instructorPrice.price)+instructorPrice.startPrice);
        }
        return 0.0;
    }

    public double getStartCost(){
        return this.startType.getCost(this);
    }

    @Override
    public Flight getTowFlight() {
        return this.towFlight==null ? null : this.towFlight;
    }

    public void propagateTowFields() {
        this.towFlight.departurePlace=this.departurePlace;
        this.towFlight.departureTime=this.departureTime;
        this.towFlight.isTowFlight= true;
        this.towFlight.paymentType=this.paymentType;
        this.towFlight.towedFlight=this;
    }

    @Override
    public void _save() {
        Logger.debug("saving glider flight");
        final PaymentStrategy paymentStrategy;
        if(this.hasDefaultPayment){
            paymentStrategy=new DefaultPayment();
        }
        else{
            paymentStrategy=new CustomPayment();
        }
        try{
            paymentStrategy.processPayment(this, billsList);
        }
        catch(NoResultException noResultEx){
            Logger.error(noResultEx.getMessage());
        }
        super._save();
    }
}
