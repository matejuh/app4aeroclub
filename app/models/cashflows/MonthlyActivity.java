package models.cashflows;

import models.User;

import javax.persistence.Entity;

/**
 * User: matej
 * Date: 16.10.12
 */
@Entity
public class MonthlyActivity extends AbstractYearlyBalance {
    public Integer month;

    public MonthlyActivity(User user, int year, int month) {
        super(user,year);
        this.month=month;
    }

    @Override
    public String toString() {
        return "MonthlyActivity{" +
                " user=" + this.user +
                ", year=" + this.year +
                ", month=" + this.month+
                ", balance=" + this.balance+
                '}';
    }
}
