package models.cashflows;

import models.User;

import javax.persistence.Entity;

/**
 * User: matej
 * Date: 3.11.12
 */
@Entity
public class YearlyBalance extends AbstractYearlyBalance {
    public YearlyBalance(User user, int year) {
        super(user, year);
    }

    public YearlyBalance(User user, Integer year, double balance) {
        super(user, year, balance);
    }

    @Override
    public String toString() {
        return "YearlyBalance{" +
                " user=" + this.user +
                ", year=" + this.year +
                ", balance=" + this.balance+
                '}';
    }
}
