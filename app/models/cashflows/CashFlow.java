package models.cashflows;

import models.User;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Min;
import play.data.validation.Required;
import play.db.jpa.JPABase;
import play.db.jpa.Model;
import utils.CustomExtensions;
import utils.DateTimeInPast;

import javax.persistence.*;
import java.util.List;

/**
 * User: matej
 * Date: 12.6.12
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class CashFlow extends Model {

    public static enum FlowType {
        income,
        expenditure,
        job,
        instructor,
        flight,
        fee
    }

    @Required
//    @IsTrue(message = "validation.notNull")
    @Min(0.0)
    public Double amount;

    @Required
    @ManyToOne
    public User user;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    @DateTimeInPast
    public DateTime date;

    public String description;

    public FlowType type;

    protected CashFlow() {
        this.date=new DateTime();
    }

    public String getDescription(){
        return this.description;
    }

    public boolean isEditable(){
        return true;
    }

    @Override
    public String toString() {
        return "cashflow: "+ CustomExtensions.formatDate(this.date)+" "+this.type+" "+this.user+" "+this.amount;
    }

    public void setUserCashFlowActivity(int koef) {
        MonthlyActivity monthlyActivity= MonthlyActivity.find("byUserAndYearAndMonth", this.user, this.date.getYear(), this.date.getMonthOfYear()).first();
        if(monthlyActivity==null){
            monthlyActivity=new MonthlyActivity(this.user,this.date.getYear(),this.date.getMonthOfYear());
        }
        monthlyActivity.balance+=(koef)*this.getAmount();
        monthlyActivity.save();

        YearlyBalance yearlyBalance= YearlyBalance.find("byUserAndYear", this.user, this.date.getYear()+1).first();
        if(yearlyBalance==null){
            YearlyBalance previous= YearlyBalance.find("user=? and year<=? order by year desc", this.user, this.date.getYear()).first();
            if(previous!=null){
                yearlyBalance=new YearlyBalance(this.user,this.date.getYear()+1,previous.balance);
            }
            else{
                yearlyBalance=new YearlyBalance(this.user,this.date.getYear()+1);
            }
        }
        yearlyBalance.balance+=(koef)*this.getAmount();
        yearlyBalance.save();

        List<YearlyBalance>balances= YearlyBalance.find("byUserAndYearGreaterThan", this.user, this.date.getYear() + 1).fetch();
        if(balances!=null && !balances.isEmpty()){
            for(YearlyBalance balance:balances){
                balance.balance+=(koef)*this.getAmount();
                balance.save();
            }
        }
    }

    public void setUserActualBalance(int koef){
        ActualBalance balance= ActualBalance.find("byUser", this.user).first();
        balance.balance+=(koef)*this.getAmount();
        balance.save();
    }

    @Override
    public void _save() {
        this.setUserActualBalance(1);
        this.setUserCashFlowActivity(1);
        super._save();
    }

    @Override
    public <T extends JPABase> T delete() {
        this.setUserActualBalance(-1);
        this.setUserCashFlowActivity(-1);
        return super.delete();
    }

    public void edit(Double price) {
        this.setUserActualBalance(-1);
        this.setUserCashFlowActivity(-1);
        this.amount=price;
        this.save();
    }

    @PostPersist
    @PostUpdate
    public void logCashFlowSave(){
        Logger.debug("Saved " + this);
    }

    @PostRemove
    public void logCashFlowDelete(){
        Logger.debug("Deleted "+this);
    }

    public static double getCost(Duration duration,double price){
        Logger.debug(CustomExtensions.format(duration)+' '+price);
        return (duration.getStandardMinutes()*price/60.0);
    }

    public abstract Double getAmount();

}
