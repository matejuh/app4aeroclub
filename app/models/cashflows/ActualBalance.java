package models.cashflows;

import models.User;

import javax.persistence.Entity;

/**
 * User: matej
 * Date: 3.11.12
 */
@Entity
public class ActualBalance extends AbstractBalance{
    public ActualBalance(User user) {
        super(user);
    }
}
