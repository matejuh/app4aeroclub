package models.cashflows;

import models.User;
import play.db.jpa.Model;

import javax.persistence.*;

/**
 * User: matej
 * Date: 11.10.12
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class AbstractBalance extends Model{
    @ManyToOne
    public User user;

    public double balance;

    public AbstractBalance(User user) {
        this.user=user;
        this.balance=0.0;
    }
}
