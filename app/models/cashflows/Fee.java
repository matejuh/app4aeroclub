package models.cashflows;

import com.google.gson.JsonObject;
import models.User;
import models.prices.FeePrice;
import org.joda.time.DateTime;
import play.Logger;

import javax.persistence.*;

/**
 * User: matej
 * Date: 18.9.12
 */
@Entity
public class Fee extends Expenditure {

    @ManyToOne
    public FeePrice price;

    public Fee() {
        this.type=FlowType.fee;
    }

    public Fee(User user, FeePrice feePrice) {
        this.type=FlowType.fee;
        this.user=user;
        this.price=feePrice;
//        this.amount=feePrice.price;
        this.date=new DateTime();
    }

    @Override
    public String getDescription() {
        return this.price.name;
    }
}
