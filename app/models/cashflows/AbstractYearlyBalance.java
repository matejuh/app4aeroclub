package models.cashflows;

import models.User;

import javax.persistence.Entity;

/**
 * User: matej
 * Date: 16.10.12
 */
@Entity
public abstract class AbstractYearlyBalance extends AbstractBalance{
    public Integer year;

    public AbstractYearlyBalance(User user, int year) {
        super(user);
        this.year = year;
    }

    public AbstractYearlyBalance(User user, Integer year, double balance) {
        super(user);
        this.year = year;
        this.balance=balance;
    }
}
