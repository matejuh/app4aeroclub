package models.cashflows;

import net.sf.oval.guard.Pre;
import play.Logger;
import play.db.jpa.JPABase;

import javax.persistence.*;

/**
 * User: matej
 * Date: 18.9.12
 */
@Entity
public class Expenditure extends CashFlow{

    public Expenditure() {
        this.type=FlowType.expenditure;
    }

    public Double getAmount(){
        return this.amount==null? null : -this.amount;
    }
}
