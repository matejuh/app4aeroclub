package models.cashflows;

import models.User;
import play.Logger;
import play.db.jpa.JPABase;

import javax.persistence.*;

/**
 * User: matej
 * Date: 18.9.12
 */
@Entity
public class Income extends CashFlow{

    public Income() {
        this.type=FlowType.income;
    }

    public Double getAmount(){
        return this.amount;
    }
}
