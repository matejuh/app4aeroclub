package models.cashflows;

import models.Flight;
import models.User;
import org.joda.time.DateTime;
import play.data.validation.Required;
import play.data.validation.Unique;
import utils.CustomExtensions;

import javax.persistence.*;

/**
 * User: matej
 * Date: 12.6.12
 */

@Entity
public class FlightBill extends Expenditure {

    @Required
    public Double flightPart;

    @ManyToOne(cascade = CascadeType.ALL)
    public Flight flight;

    public FlightBill() {
        this.type=FlowType.flight;
    }

    public FlightBill(Flight flight, User user, double flightPart,double flightCost) {
        this.type=FlowType.flight;
        this.amount=flightCost*(flightPart/100.0);
        this.user=user;
        this.date=flight.departureTime==null ? new DateTime() : flight.departureTime;
        this.flight=flight;
        this.flightPart=flightPart;
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public String getDescription() {
        return CustomExtensions.formatDateTime(this.flight.departureTime)+" "+this.flight.airplane.sign;
    }

    @Override
    public String toString(){
        return super.toString()+" "+this.flightPart;
    }

}
