package models.cashflows;

import models.Flight;
import utils.CustomExtensions;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * User: matej
 * Date: 22.9.12
 */
@Entity
public class InstructorWork extends Income{

    @OneToOne(cascade = CascadeType.ALL)
    public Flight instructorFlight;

    public InstructorWork() {
        this.type=FlowType.instructor;
    }

    public InstructorWork(Flight flight, double instructorCost) {
        this.type=FlowType.instructor;
        this.instructorFlight=flight;
        this.amount=instructorCost;
        this.date=flight.departureTime;
        this.user=flight.secondCrewMember;
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public String getDescription() {
        return CustomExtensions.formatDateTime(this.instructorFlight.departureTime)+" "+this.instructorFlight.airplane.sign;
    }
}
