package models.cashflows;

import models.User;
import models.prices.JobPrice;
import org.hibernate.annotations.Type;
import org.joda.time.Duration;
import play.Logger;
import play.data.validation.Required;
import play.libs.I18N;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;

/**
 * User: matej
 * Date: 12.6.12
 */

@Entity
public class Job extends Income {

    @Required
    @Type(type="org.joda.time.contrib.hibernate.PersistentDuration")
    public Duration duration;

    @Required
    @ManyToOne
    public JobPrice price;

    public Job() {
        this.type=FlowType.job;
    }

}
