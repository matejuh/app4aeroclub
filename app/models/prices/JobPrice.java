package models.prices;

import play.data.validation.Required;
import play.data.validation.Unique;

import javax.persistence.Entity;

/**
 * User: matej
 * Date: 17.9.12
 */
@Entity
public class JobPrice extends AbstractPrice {

    @Required
    @Unique
    public String jobType;
}
