package models.prices;

import play.data.validation.Min;
import play.data.validation.Required;

import javax.persistence.Entity;

/**
 * User: matej
 * Date: 17.10.12
 */
@Entity
public class InstructorPricePrice extends Price {
    @Required
    @Min(0.0)
    public Double startPrice;
}
