package models.prices;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.data.validation.Valid;
import play.db.jpa.Model;
import utils.AirplanePriceConditionComparator;
import utils.DateTimeInFuture;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

/**
 * User: matej
 * Date: 23.10.12
 */
@Entity
public class AirplanePrice extends Model{
    @ManyToOne(cascade = CascadeType.PERSIST)
    @Unique("date")
    public AirplanePayment parent;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    @Required
    @DateTimeInFuture
    public DateTime date;

    @ManyToMany(cascade=CascadeType.PERSIST)
    public List<AirplanePricePrice> prices;

    public List<AirplanePricePrice> getPrices() {
        Collections.sort(this.prices,new AirplanePriceConditionComparator());
        return this.prices;
    }

    public void setPrices(List<AirplanePricePrice> prices) {
        this.prices = prices;
    }
}
