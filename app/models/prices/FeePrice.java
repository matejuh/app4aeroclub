package models.prices;

import org.joda.time.DateTime;
import play.Logger;
import play.data.validation.Required;

import javax.persistence.Entity;
import java.util.List;

/**
 * User: matej
 * Date: 17.9.12
 */
@Entity
public class FeePrice extends AbstractPrice {

    @Required
    public String name;

}
