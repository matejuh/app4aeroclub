package models.prices;

import play.data.validation.Min;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Model;

import javax.persistence.Entity;

/**
 * User: matej
 * Date: 23.10.12
 */
@Entity
public class AirplanePricePrice extends Model{
    @Required(message = "Price required.")
    @Min(0.0)
    public Double price;

    @Required
    @Min(0)
    @Unique("price")
    public Integer cond;
}
