package models.prices;

import models.cashflows.CashFlow;
import org.joda.time.Duration;
import play.data.validation.Required;

import javax.persistence.Entity;

/**
 * User: matej
 * Date: 21.10.12
 */
@Entity
public class TowPricePrice extends Price {
    public static enum TowPayment {
        hour{
            public double getCost(Duration towDuration,TowPricePrice price){
                return CashFlow.getCost(towDuration, price.price);
            }
        },
        start{
            public double getCost(Duration towDuration,TowPricePrice price){
                return price.price;
            }
        };

        public abstract double getCost(Duration towDuration,TowPricePrice price);
    }

    @Required(message = "Tow payment required, you must select it! Click on corresponding button.")
    public TowPayment towPayment;
}
