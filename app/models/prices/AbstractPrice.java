package models.prices;

import org.joda.time.DateTime;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;

/**
 * User: matej
 * Date: 17.9.12
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class AbstractPrice extends Model{

    @OneToMany(mappedBy = "parent")
    public List<Price>priceHistory;

    public List<Price> getPriceHistory() {
        return Price.find("parent=? order by date desc", this).fetch();
    }

    public Price getPrice(DateTime date) {
        return Price.find("parent=? and date<=? order by date desc",this,date).first();
    }

    public Price getActualPrice() {
        return this.getPrice(DateTime.now());
    }
}
