package models.prices;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import play.data.binding.As;
import play.data.validation.Min;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Model;
import utils.CustomExtensions;
import utils.DateTimeInFuture;

import javax.persistence.*;

/**
 * User: matej
 * Date: 16.10.12
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Price extends Model{
    @ManyToOne(cascade = CascadeType.PERSIST)
    @Unique("date")
    public AbstractPrice parent;

    @Required(message = "Price required.")
    @Min(0.0)
    public Double price;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    @Required
    @DateTimeInFuture
    public DateTime date;

    @Override
    public String toString() {
        return CustomExtensions.formatDate(this.date)+" "+this.price;
    }
}
