package models.prices;

import models.PaymentType;
import org.joda.time.DateTime;
import utils.CustomExtensions;

import javax.persistence.Entity;
import javax.persistence.NoResultException;

/**
 * User: matej
 * Date: 17.9.12
 */
@Entity
public class WinchPrice extends FlightRelatedPrice {

    public WinchPrice() {
        super();
    }

    public WinchPrice(PaymentType paymentType) {
        super(paymentType);
    }

    public static Price getWinchPrice(PaymentType paymentType, DateTime date) throws NoResultException {
        WinchPrice winchPrice=WinchPrice.find("paymentType=?",paymentType).first();
        Price price=Price.find("parent=? and date<=? order by date desc",winchPrice,date).first();
        if(price==null){
            throw new NoResultException("Winch price valid for date: "+ CustomExtensions.formatDate(date)+" payment type: "+paymentType+" not found.");
        }
        return price;
    }
}
