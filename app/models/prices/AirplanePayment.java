package models.prices;


import models.Airplane;
import models.PaymentType;
import org.joda.time.DateTime;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;

/**
 * User: matej
 * Date: 15.9.12
 */
@Entity
public class AirplanePayment extends Model {

    @Required
    @ManyToOne
    @Unique("airplane")
    public PaymentType paymentType;

    @ManyToOne
    public Airplane airplane;

    @OneToMany(mappedBy = "parent")
    public List<AirplanePrice> priceHistory;

    public AirplanePayment() {
    }

    public AirplanePayment(Airplane airplane) {
        this.airplane=airplane;
    }

    public AirplanePayment(Airplane airplane, PaymentType paymentType) {
        this.airplane=airplane;
        this.paymentType=paymentType;
    }

    public List<AirplanePrice> getPriceHistory() {
        return AirplanePrice.find("parent=? order by date desc", this).fetch();
    }

}
