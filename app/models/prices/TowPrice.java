package models.prices;

import models.Aircraft;
import models.PaymentType;
import org.joda.time.Duration;
import play.data.validation.Required;

import javax.persistence.*;

/**
 * User: matej
 * Date: 16.9.12
 */
@Entity
public class TowPrice extends FlightRelatedPrice {

    @Required
    @ManyToOne
    public Aircraft towPlane;

    public TowPrice(Aircraft towPlane) {
        super();
        this.towPlane=towPlane;
    }

    public TowPrice(Aircraft towPlane, PaymentType paymentType) {
        super(paymentType);
        this.towPlane=towPlane;
    }
}
