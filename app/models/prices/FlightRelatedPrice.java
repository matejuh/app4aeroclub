package models.prices;

import models.PaymentType;
import play.data.validation.*;

import javax.persistence.*;

/**
 * User: matej
 * Date: 12.6.12
 */

@Entity
public abstract class FlightRelatedPrice extends AbstractPrice {

    @Required
    @ManyToOne
    public PaymentType paymentType;

    protected FlightRelatedPrice(){
        super();
    }

    protected FlightRelatedPrice(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName()+" "+this.paymentType.name;
    }

}
