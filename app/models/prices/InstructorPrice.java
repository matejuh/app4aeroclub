package models.prices;

import models.PaymentType;
import org.joda.time.DateTime;
import play.data.validation.Required;
import play.data.validation.Unique;
import utils.CustomExtensions;

import javax.persistence.Entity;
import javax.persistence.NoResultException;

/**
 * User: matej
 * Date: 3.8.12
 */
@Entity
public class InstructorPrice extends FlightRelatedPrice {

    public static enum FlightType {
        aircraft,glider
    }

    @Required
    @Unique("paymentType")
    public FlightType flightType;

    public InstructorPrice() {
        super();
    }

    public InstructorPrice(FlightType flightType, PaymentType paymentType) {
        super(paymentType);
        this.flightType=flightType;
    }

    @Override
    public Price getPrice(DateTime date) {
        return InstructorPricePrice.find("parent=? and date<=? order by date desc",this,date).first();
    }

    @Override
    public Price getActualPrice() {
        return this.getPrice(DateTime.now());
    }

    public static InstructorPricePrice getInstructorPrice(PaymentType paymentType, FlightType flightType, DateTime date) throws NoResultException{
        InstructorPrice instructorPrice=find("byPaymentTypeAndFlightType",paymentType,flightType).first();
        InstructorPricePrice price=(InstructorPricePrice)instructorPrice.getPrice(date);
        if(price==null){
            throw new NoResultException(CustomExtensions.capFirst(flightType)+" instructor price valid for date: "+ CustomExtensions.formatDate(date)+" payment type: "+paymentType+" not found.");
        }
        return price;
    }
}
