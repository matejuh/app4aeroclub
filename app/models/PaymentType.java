package models;

import models.prices.*;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.JPABase;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;

/**
 * User: matej
 * Date: 1.7.12
 */

@Entity
public class PaymentType extends Model {
    @Unique
    @Required
    public String name;

    @Required
    public boolean isActive=true;

    @OneToMany(mappedBy = "paymentType",cascade = CascadeType.ALL)
    public List<FlightRelatedPrice>prices;

    public PaymentType(){
        this.isActive=true;
    }

    public PaymentType(String name) {
        this.name=name;
        this.isActive=true;
    }

    @Override
    public String toString(){
        return this.name;
    }

    @Override
    public <T extends JPABase> T delete() {
        /**
         * DONT DELETE!!!!!
         **/

//        Logger.info("delete payment type");
//        long references=FlightRelatedPrice.count("paymentType=?",this);
//        if(references>0){
//            this.isActive=false;
//            WinchPrice actualWinchPrice=WinchPrice.findActualWinchPrice(this);
//            //don't have to delete price which has been already deleted
//            if(actualWinchPrice!=null && actualWinchPrice.isActive){
//                WinchPrice deletedWinchPrice=new WinchPrice(actualWinchPrice);
//                deletedWinchPrice.isActive=false;
//                deletedWinchPrice.save();
//            }
//            List<InstructorPrice>instructorPrices=InstructorPrice.findActualInstructorPrices();
//            for(InstructorPrice instructorPrice:instructorPrices){
//                InstructorPrice deletedInstrutorPrice=new InstructorPrice(instructorPrice);
//                deletedInstrutorPrice.isActive=false;
//                deletedInstrutorPrice.save();
//            }
//
//            //Tow planes with given payment type
//            List<Aircraft>towPlanes=TowPrice.find("select towPlane from TowPrice towPrice where paymentType=? group by towPlane",this).fetch();
//            //Get latest tow prices
//            List<TowPrice>towPrices=new ArrayList<TowPrice>();
//            for(Aircraft towPlane:towPlanes){
//                towPrices.add(towPlane.findActualTowPrice(this));
//            }
//            for(TowPrice towPrice:towPrices){
//                TowPrice deletedTowPrice=new TowPrice(towPrice);
//                deletedTowPrice.isActive=false;
//                deletedTowPrice.save();
//            }
//
//            //Airplanes with given payment type
//            List<Airplane>airplanes= AirplanePrice.find("select airplane from AirplanePrice airplanePrice where paymentType=? and hours=? group by airplane",this,0).fetch();
//            //Get latest airplane prices
//            List<AirplanePrice>airplanePrices=new ArrayList<AirplanePrice>();
//            for(Airplane airplane:airplanes){
//                airplanePrices.addAll(airplane.findActualPrices(this));
//            }
//            for(AirplanePrice airplanePrice:airplanePrices){
//                if(airplanePrice!=null){
//                    AirplanePrice deletedAirplanePrice=new AirplanePrice(airplanePrice);
//                    deletedAirplanePrice.isActive=false;
//                    deletedAirplanePrice.save();
//                }
//
//            }
//            this.save();
//        }
//        else{
//            super.delete();
//        }
        this.isActive=false;
        this.save();
        return null;
    }


    public static List<PaymentType> findAllActive(){
        return PaymentType.find("byIsActive",true).fetch();
    }
}
