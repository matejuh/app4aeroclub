package models;

import models.prices.TowPrice;
import models.prices.TowPricePrice;
import org.joda.time.DateTime;
import play.data.validation.Valid;
import utils.CustomExtensions;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User: matej
 * Date: 12.6.12
 */

@Entity
@DiscriminatorValue("Aircraft")
@Table(name = "Aircraft")
public class Aircraft extends Airplane {

    public boolean isTowPlane=false;

    @Valid
    @OneToMany(mappedBy = "towPlane",cascade = CascadeType.ALL)
    public List<TowPrice> towPriceList;

    public Aircraft() {
        super();
    }

    public static List<Aircraft> findAllTowPlanes() {
        return Aircraft.find("isTowPlane",true).fetch();
    }

    public List<TowPrice> findTowPriceHistory(PaymentType paymentType) {
        return TowPrice.find("towPlane=? and paymentType=? order by date desc",this,paymentType).fetch();
    }

    public List<TowPricePrice> getActualTowPrices(){
        List<TowPricePrice>prices=new ArrayList<TowPricePrice>();
        List<TowPrice>towPrices=TowPrice.find("byTowPlane",this).fetch();
        for(TowPrice towPrice:towPrices){
            TowPricePrice price=TowPricePrice.find("parent=? and date<=? order by date desc",towPrice,DateTime.now()).first();
            if(price!=null){
                prices.add(price);
            }
        }
        return prices.size()>0 ? prices : null;
    }

    public TowPricePrice getTowPrice(PaymentType paymentType, DateTime date) throws NoResultException {
        TowPrice towPrice=TowPrice.find("byTowPlaneAndPaymentType",this,paymentType).first();
        TowPricePrice price=TowPricePrice.find("parent=? and date<=? order by date desc",towPrice,date).first();
        if(price==null){
            throw new NoResultException("Tow price for airplane "+this+" payment type "+paymentType+" valid for "+ CustomExtensions.formatDate(date)+"not found");
        }
        return price;
    }
}
