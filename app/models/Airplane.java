package models;

import models.prices.AirplanePayment;
import models.prices.AirplanePrice;
import models.prices.AirplanePricePrice;
import models.prices.Price;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jpa.JPA;
import play.db.jpa.Model;
import utils.AirplanePriceConditionComparator;
import utils.CustomExtensions;

import javax.persistence.*;
import java.util.*;

/**
 * User: matej
 * Date: 10.6.12
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name="Category")
@Table(name = "Airplanes")
public class Airplane extends Model {
    @Required
    public String sign;

    @Required
    public String type;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    public DateTime airworthiness;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    public DateTime radioLicence;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    public DateTime annualInspection;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    public DateTime responsibilityInsurance;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    public DateTime cashInsurance;

    @OneToMany(mappedBy = "airplane",cascade = CascadeType.ALL)
    public List<AirplanePayment> priceList;

    @Override
    public String toString(){
        return this.type+" "+this.sign;
    }

    /**
     * Get airplane hour price according to params
     * @param yearlyFlyingHours
     * @param paymentType
     * @param date
     * @return rent hourly price according to given params
     * @throws NoResultException
     */
    public Double getHourPrice(int yearlyFlyingHours, PaymentType paymentType, DateTime date) throws NoResultException{
        AirplanePayment airplanePayment=AirplanePayment.find("byAirplaneAndPaymentType",this,paymentType).first();
        AirplanePrice airplanePrice=AirplanePrice.find("parent=? and date<=? order by date desc",airplanePayment,date).first();
        if(airplanePrice==null){
            throw new NoResultException("Airplane price for airplane: "+this.sign+" date: "+ CustomExtensions.formatDate(date)+" payment type: "+paymentType+" user yearly flying hours: "+yearlyFlyingHours+" not found.");
        }
        /*dont erase*/
//        List<Long>ids=new ArrayList<Long>();
//        for(AirplanePricePrice airplanePricePrice:airplanePrice.prices){
//            ids.add(airplanePricePrice.id);
//        }
//        AirplanePricePrice price=AirplanePricePrice.find("cond<=(?1) and id in (?2) order by cond",yearlyFlyingHours,ids).first();
        Collections.sort(airplanePrice.prices,new AirplanePriceConditionComparator());
        int i=0;
        while(airplanePrice.prices.get(i).cond<yearlyFlyingHours){
            i++;
        }
        AirplanePricePrice price=airplanePrice.prices.get(i);
        Logger.debug("Rental price: "+this.sign+", payment type: "+paymentType+", pilot flying hours: "+yearlyFlyingHours+",price: "+price.price+", condition: "+price.cond);
        return price.price;
    }

    /**
     * Get actual prices for price list
     * @return prices
     */
    public List<AirplanePrice> getActualPrices(){
        List<AirplanePrice>airplanePrices=new ArrayList<AirplanePrice>();
        List<AirplanePayment> airplanePayments=AirplanePayment.find("byAirplane",this).fetch();
        if(airplanePayments!=null){
            for(AirplanePayment airplanePayment:airplanePayments){
                AirplanePrice airplanePrice=AirplanePrice.find("parent=? and date<=? order by date desc order",airplanePayment,DateTime.now()).first();
                if(airplanePrice!=null){
                    airplanePrices.add(airplanePrice);
                }
            }
        }
        return airplanePrices.size()>0 ? airplanePrices : null;
    }
}
