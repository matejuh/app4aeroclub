package models;

import models.cashflows.*;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Years;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Email;
import play.data.validation.Phone;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.JPA;
import play.db.jpa.Model;
import utils.DateTimeInPast;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * User: matej
 * Date: 10.6.12
 */

@Entity
public class User extends Model {

    @Required
    @Email
    @Unique
    public String email;
    @Required
    public String firstName;
    @Required
    @NotNull
    public String lastName;

    public String password;

    @OneToOne(cascade = CascadeType.ALL)
    public ActualBalance actualBalance;

    @OneToOne(cascade = CascadeType.ALL)
    public Address address;

    @Phone
    public String phone;

    public String im;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    public List<Licence>licences;

    public boolean isActive=false;

    public boolean isEnabled=false;

    @ManyToOne
    public PaymentType paymentType;

    @Required
    public boolean wantsSendNews=true;

    @Required
    public boolean wantsBeInformed=false;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    @Required
    @DateTimeInPast
    public DateTime birthday;

    @Transient
    public Double balance;

    @Transient
    public int age;

    public User() {
        this.actualBalance =new ActualBalance(this);
    }
//    @OneToMany(mappedBy="pilot", cascade= CascadeType.ALL)
//    @DontSerialize
//    public List<Flight> pilotFlights;

//    @OneToMany(mappedBy = "secondCrewMember", cascade = CascadeType.ALL)
//    @DontSerialize
//    public List<Flight> secondCrewMemberFlights;

//    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
//    public List<CashFlow> bills;

//    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
//    public List<FlightBill> flightBills;

//    @Transient
//    @DontSerialize
//    public List<Flight> Flights;

//    @Override
//    public void _save() {
//        super._save();
//        Logger.debug("save");
//        Balance actualBalance=new Balance(this);
//        this.actualBalance=actualBalance;
//        actualBalance.save();
//
//    }

    @Override
    public String toString(){
        return this.id+" "+this.firstName +" "+this.lastName;
    }

    public int getHoursPerYear(DateTime time, String table) {
        DateTime yearStart=new DateTime(time.getYear(),1,1,0,0);
        DateTime yearEnd=new DateTime(time.getYear()+1,1,1,0,0);
        List<Duration>durations=AircraftFlight.find("select distinct flight.duration from "+table+" flight join flight.pilot as pilot left join flight.secondCrewMember as secondCrewMember where pilot=? or secondCrewMember=? and flight.departureTime between ? and ?",this,this,yearStart,yearEnd).fetch();

        Duration totalDuration=new Duration(0);

        for(Duration duration:durations){
            totalDuration.plus(duration);
        }

        long yearlyHours=totalDuration.getStandardHours();
        if (yearlyHours < Integer.MIN_VALUE || yearlyHours > Integer.MAX_VALUE) {
            throw new IllegalArgumentException
                    (yearlyHours + " cannot be cast to int without changing its value.");
        }

        return (int)yearlyHours;
    }

    public Double getBalance(int year,int month){
        YearlyBalance yearlyBalance= YearlyBalance.find("user=? and year<=? order by year desc", this, year).first();
        Double bal= (Double)JPA.em().createQuery("select sum(a.balance) from MonthlyActivity a where a.user=:user and a.year=:year and a.month<=:month").setParameter("user",this).setParameter("year",year).setParameter("month",month).getSingleResult();
        return (bal==null ? 0.0 : bal)+(yearlyBalance==null ? 0.0 : yearlyBalance.balance);
    }

    public int getAge(){
        Years age = Years.yearsBetween(this.birthday, DateTime.now());
        return age.getYears();
    }

}
