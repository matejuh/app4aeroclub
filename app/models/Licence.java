package models;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Model;
import utils.CustomExtensions;
import utils.DateTimeInFuture;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * User: matej
 * Date: 27.10.12
 */
@Entity
public class Licence extends Model{

    public static enum LicenceType {
        AFIS,
        mechanic,
        PPL,
        CPL,
        GLD,
        medical
    }
    @Required
    @ManyToOne(cascade = CascadeType.ALL)
    public User user;

    @Required
    @Unique("user")
    public LicenceType type;

    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    @As("dd.MM.yyyy")
    @Required
    @DateTimeInFuture
    public DateTime validity;

    @Required
    public String number;
}
