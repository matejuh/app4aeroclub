package models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * User: matej
 * Date: 12.6.12
 */

@Entity
@DiscriminatorValue("Glider")
@Table(name = "Gliders")
public class Glider extends Airplane{
    //TODO enable add second crew member only ifDoubleSeater
    public boolean isDoubleSeater;
}
