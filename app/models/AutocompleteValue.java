package models;

/**
 * User: matej
 * Date: 7.7.12
 */
public class AutocompleteValue {

    private Long value;
    private String label;

    public AutocompleteValue(final Long id, final String label) {
        this.value = id;
        this.label = label;
    }
}
