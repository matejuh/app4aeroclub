package utils;

import com.google.gson.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import play.libs.I18N;

import java.lang.reflect.Type;

/**
 * User: matej
 * Date: 18.9.12
 */
public class DateTimeSerializer implements JsonSerializer<DateTime> {

    @Override
    public JsonElement serialize(DateTime dateTime, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(CustomExtensions.formatDateTime(dateTime));
    }
}
