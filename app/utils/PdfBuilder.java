package utils;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import models.Flight;
import models.User;
import models.cashflows.CashFlow;
import models.cashflows.Job;
import play.Logger;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/**
 * User: matej
 * Date: 1.11.12
 */
public class PdfBuilder {
    private List list;
    private String name;
    private Document document;
    private static String title="";
    private static float width=527;

    private Font titleFont=  new Font(Font.FontFamily.HELVETICA,14,Font.BOLD);
    private Font fontHeader = new Font(Font.FontFamily.HELVETICA,10,Font.BOLD);
    private Font fontCell = new Font(Font.FontFamily.HELVETICA,10);
    private static Font fontHead = new Font(Font.FontFamily.HELVETICA,8);
    private Font posFont=new Font(Font.FontFamily.HELVETICA,10, Font.NORMAL,new BaseColor(26,141,27));
    private Font negFont=new Font(Font.FontFamily.HELVETICA,10, Font.NORMAL,new BaseColor(225,36,32));

    /**
     * Create a header table with page X of Y
     * @param x the page number
     * @param y the total number of pages
     * @return a table that can be used as header
     */
    public static PdfPTable getHeaderTable(int x, int y) {
        PdfPTable table = new PdfPTable(2);
        table.setTotalWidth(width);
        table.setLockedWidth(true);
        table.getDefaultCell().setFixedHeight(14);
        table.getDefaultCell().setBorder(Rectangle.BOTTOM);
        table.getDefaultCell().setPhrase(new Phrase(title,fontHead));
        table.addCell(table.getDefaultCell());
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.getDefaultCell().setPhrase(new Phrase(String.format("Page %d of %d", x, y),fontHead));
        table.addCell(table.getDefaultCell());
        return table;
    }



    public PdfBuilder(List list,String name) {
        this.list = list;
        this.name=name;
    }

    public File createContactsPdf(){
        File f=null;
        try {
            //set pdf document
            /** for rotation*/
            this.document = new Document(PageSize.A4.rotate(),30, 30, 60, 30);
            width=785;
            //this.document = new Document(PageSize.A4, 30, 30, 30, 30);
            f=File.createTempFile(name,".pdf");
            f.deleteOnExit();
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            PdfWriter.getInstance(document, baos);
            title="Contacts";
            document.open();
            //create document
            createContactsTable(document);
            document.close();

            // SECOND PASS, ADD THE HEADER

            // Create a reader
            PdfReader reader = new PdfReader(baos.toByteArray());
            // Create a stamper
            PdfStamper stamper
                    = new PdfStamper(reader, new FileOutputStream(f));
            // Loop over the pages and add a header to each page
            int n = reader.getNumberOfPages();
            for (int i = 1; i <= n; i++) {
                getHeaderTable(i, n).writeSelectedRows(
                        0, -1, 28, 560, stamper.getOverContent(i));
            }
            // Close the stamper
            stamper.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;
    }

    public File createBalancesPdf(String monthYear) {
        File f=null;
        try {
            //set pdf document
            /** for rotation*/
            //this.document = new Document(PageSize.A4.rotate());
            this.document = new Document(PageSize.A4, 36, 36, 65, 36);
            width=527;
            f=File.createTempFile(name,".pdf");
            f.deleteOnExit();
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            PdfWriter writer=PdfWriter.getInstance(document,baos);
            document.open();
            //create document
            if(monthYear==null){
                title="Actual balances";
            }
            else{
                title="Balances "+monthYear;
            }
            createBalancesTable(document);
            document.close();

            // SECOND PASS, ADD THE HEADER

            // Create a reader
            PdfReader reader = new PdfReader(baos.toByteArray());
            // Create a stamper
            PdfStamper stamper
                    = new PdfStamper(reader, new FileOutputStream(f));
            // Loop over the pages and add a header to each page
            int n = reader.getNumberOfPages();
            for (int i = 1; i <= n; i++) {
                getHeaderTable(i, n).writeSelectedRows(
                        0, -1, 34, 803, stamper.getOverContent(i));
            }
            // Close the stamper
            stamper.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;
    }

    public File createFlightsPdf(boolean filter) {
        File f=null;
        try {
            //set pdf document
            /** for rotation*/
            this.document = new Document(PageSize.A4.rotate(),30, 30, 60, 30);
            width=785;
            //this.document = new Document(PageSize.A4, 30, 30, 30, 30);
            f=File.createTempFile(name,".pdf");
            f.deleteOnExit();
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            PdfWriter.getInstance(document, baos);
            title="Flights";
            document.open();
            //create document
            createFlightsTable(document,filter);
            document.close();

            // SECOND PASS, ADD THE HEADER

            // Create a reader
            PdfReader reader = new PdfReader(baos.toByteArray());
            // Create a stamper
            PdfStamper stamper
                    = new PdfStamper(reader, new FileOutputStream(f));
            // Loop over the pages and add a header to each page
            int n = reader.getNumberOfPages();
            for (int i = 1; i <= n; i++) {
                getHeaderTable(i, n).writeSelectedRows(
                        0, -1, 28, 560, stamper.getOverContent(i));
            }
            // Close the stamper
            stamper.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;
    }

    public File createCashFlowsPdf() {
        File f=null;
        try {
            //set pdf document
            /** for rotation*/
            this.document = new Document(PageSize.A4.rotate(),30, 30, 60, 30);
            width=785;
            f=File.createTempFile(name,".pdf");
            f.deleteOnExit();
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            PdfWriter writer=PdfWriter.getInstance(document,baos);
            document.open();
            //create document
            title="Cash-flow";
            createCashFlowsTable(document);
            document.close();

            // SECOND PASS, ADD THE HEADER

            // Create a reader
            PdfReader reader = new PdfReader(baos.toByteArray());
            // Create a stamper
            PdfStamper stamper
                    = new PdfStamper(reader, new FileOutputStream(f));
            // Loop over the pages and add a header to each page
            int n = reader.getNumberOfPages();
            for (int i = 1; i <= n; i++) {
                getHeaderTable(i, n).writeSelectedRows(
                        0, -1, 28, 560, stamper.getOverContent(i));
            }
            // Close the stamper
            stamper.close();
            // Close the stamper
            stamper.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;

    }

    public File createJobsPdf() {
        File f=null;
        try {
            //set pdf document
            /** for rotation*/
            this.document = new Document(PageSize.A4.rotate(),30, 30, 60, 30);
            width=785;
            f=File.createTempFile(name,".pdf");
            f.deleteOnExit();
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            PdfWriter writer=PdfWriter.getInstance(document,baos);
            document.open();
            //create document
            title="Jobs";
            createJobsTable(document);
            document.close();

            // SECOND PASS, ADD THE HEADER

            // Create a reader
            PdfReader reader = new PdfReader(baos.toByteArray());
            // Create a stamper
            PdfStamper stamper
                    = new PdfStamper(reader, new FileOutputStream(f));
            // Loop over the pages and add a header to each page
            int n = reader.getNumberOfPages();
            for (int i = 1; i <= n; i++) {
                getHeaderTable(i, n).writeSelectedRows(
                        0, -1, 28, 560, stamper.getOverContent(i));
            }
            // Close the stamper
            stamper.close();
            // Close the stamper
            stamper.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;
    }

    private void createJobsTable(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(new float[] { 1.6f,5,5,3,1.6f,1,2});

        table.setWidthPercentage(100f);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("Date",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("User",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Description",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Type",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Price/h",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Dur.",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Total",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        table.setHeaderRows(1);

        for(Job job:(List<Job>)list){
            table.addCell(new Phrase(CustomExtensions.formatDate(job.date),fontCell));
            table.addCell(new Phrase(job.user.toString(),fontCell));
            table.addCell(new Phrase(job.description,fontCell));
            table.addCell(new Phrase(job.price.jobType,fontCell));
            cell=new PdfPCell(new Phrase(job.price.getPrice(job.date).price.toString(),fontCell));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            cell=new PdfPCell(new Phrase(CustomExtensions.format(job.duration),fontCell));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            Font tmpFont=fontCell;
            if(job.amount<0){
                tmpFont=negFont;
            }
            else{
                tmpFont=posFont;
            }
            cell = new PdfPCell(new Phrase(CustomExtensions.format(job.amount),tmpFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
        }

        document.add(table);
    }

    private void createCashFlowsTable(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(new float[] { 1,5,2,5,1 });

        table.setWidthPercentage(100f);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("Date",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("User",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Type",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Description",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Amount",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        table.setHeaderRows(1);

        for(CashFlow cashFlow:(List<CashFlow>)list){
            table.addCell(new Phrase(CustomExtensions.formatDate(cashFlow.date),fontCell));
            table.addCell(new Phrase(cashFlow.user.toString(),fontCell));
            table.addCell(new Phrase(cashFlow.type.toString(),fontCell));
            table.addCell(new Phrase(cashFlow.getDescription(),fontCell));
            Font tmpFont=fontCell;
            if(cashFlow.amount<0){
                tmpFont=negFont;
            }
            else{
                tmpFont=posFont;
            }
            cell = new PdfPCell(new Phrase(CustomExtensions.format(cashFlow.amount),tmpFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
        }

        document.add(table);
    }

    private void createFlightsTable(Document document,boolean filter) throws DocumentException{

        PdfPTable table = new PdfPTable(new float[] { 1.5f,2,7,8,3,1,1,1,0.5f});

        table.setWidthPercentage(100f);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("Id",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Date",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Airplane",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Crew",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Task",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Dep.",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Arr.",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Dur.",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        table.setHeaderRows(1);

        int index=0;
        for(Flight flight:(List<Flight>)list){
            cell = new PdfPCell(new Phrase(flight.id.toString(),fontCell));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            table.addCell(new Phrase(CustomExtensions.formatDate(flight.departureTime),fontCell));
            table.addCell(new Phrase(flight.airplane.toString(),fontCell));
            table.addCell(new Phrase(flight.pilot.toString()+ (flight.secondCrewMember==null ? "" : " "+flight.secondCrewMember+(flight.isFlightWithInstructor ? " instr." : "")),fontCell));
            table.addCell(new Phrase(flight.task,fontCell));
            table.addCell(new Phrase(CustomExtensions.formatTime(flight.departureTime),fontCell));
            table.addCell(new Phrase(CustomExtensions.formatTime(flight.arrivalTime),fontCell));
            table.addCell(new Phrase(CustomExtensions.format(flight.duration),fontCell));
            if(!filter){
                if(flight.towFlight!=null){
                    if(index+1<list.size() && ((Flight)list.get(index+1)).towFlight!=null && ((Flight)list.get(index+1)).towFlight.id.equals(flight.id)){
                        cell = new PdfPCell(new Phrase("\\"));
                    }
                    else{
                        cell = new PdfPCell(new Phrase("/"));
                    }
                    table.addCell(cell);
                }
                else{
                    table.addCell(new Phrase("",fontCell));
                }
            }
            else{
                table.addCell(new Phrase("",fontCell));
            }
            index++;
        }

        document.add(table);
    }

    private void createContactsTable(Document document) throws DocumentException {

        PdfPTable table = new PdfPTable(new float[] { 2, 1, 2, 4 });

        table.setWidthPercentage(100f);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("Member",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Phone",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Email",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Address",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        table.setHeaderRows(1);

        for(User user:(List<User>)list){
            table.addCell(new Phrase(user.id.toString()+" "+user.lastName+" "+user.firstName,fontCell));
            table.addCell(new Phrase(user.phone,fontCell));
            table.addCell(new Phrase(user.email,fontCell));
            table.addCell(new Phrase(user.address.street+" "+user.address.houseNumber+", "+user.address.town+" "+user.address.postalCode,fontCell));
        }

        document.add(table);
    }

    private void createBalancesTable(Document document) throws DocumentException {
        boolean getActual=false;

        PdfPTable table = new PdfPTable(new float[] { 1,8, 3 });

        table.setWidthPercentage(100f);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("#",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Name",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Balance",fontHeader));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        table.setHeaderRows(1);

        for(User user:(List<User>)list){
            cell = new PdfPCell(new Phrase(user.id.toString(),fontCell));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
            table.addCell(new Phrase(user.lastName+" "+user.firstName,fontCell));
            Font tmpFont=fontCell;
            if(user.balance<0){
                tmpFont=negFont;
            }
            else{
                tmpFont=posFont;
            }
            cell = new PdfPCell(new Phrase(CustomExtensions.format(user.balance),tmpFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

            table.addCell(cell);
        }

        document.add(table);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }



}
