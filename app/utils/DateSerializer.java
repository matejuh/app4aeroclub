package utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.joda.time.DateTime;

import java.lang.reflect.Type;

/**
 * User: matej
 * Date: 18.9.12
 */
public class DateSerializer implements JsonSerializer<DateTime> {
    @Override
    public JsonElement serialize(DateTime dateTime, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(CustomExtensions.formatDate(dateTime));
    }
}
