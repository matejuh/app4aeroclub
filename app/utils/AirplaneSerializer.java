package utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import models.Aircraft;
import models.Airplane;
import play.Logger;

import java.lang.reflect.Type;

/**
 * User: matej
 * Date: 3.11.12
 */
public class AirplaneSerializer implements JsonSerializer<Airplane> {
    @Override
    public JsonElement serialize(Airplane airplane, Type type, JsonSerializationContext jsonSerializationContext) {
        Logger.debug("serialize airplane");
        JsonObject obj=new JsonObject();
        obj.addProperty("id", airplane.id);
        obj.addProperty("label",airplane.toString());
        return obj;
    }
}
