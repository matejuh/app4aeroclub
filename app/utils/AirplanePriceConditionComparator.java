package utils;

import models.prices.AirplanePricePrice;
import models.prices.FlightRelatedPrice;

import java.util.Comparator;

/**
 * User: matej
 * Date: 14.8.12
 */
public class AirplanePriceConditionComparator implements Comparator<AirplanePricePrice> {

    @Override
    public int compare(AirplanePricePrice o1, AirplanePricePrice o2) {
        return (o1.cond).compareTo(o2.cond);
    }
}
