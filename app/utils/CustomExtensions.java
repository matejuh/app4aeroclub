package utils;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import play.Logger;
import play.i18n.Lang;
import play.libs.I18N;
import play.templates.JavaExtensions;

import java.util.Currency;
import java.util.Locale;
import java.text.*;

/**
 * User: matej
 * Date: 14.8.12
 */
public class CustomExtensions extends JavaExtensions {

    /** joda.time.Duration formatting as HH:MM in template */
    public static String format(Duration duration) {

        PeriodFormatter durationFormatter = new PeriodFormatterBuilder()
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendHours()
                .appendSeparator(":")
                .appendMinutes()
                .toFormatter();

        return durationFormatter.print(duration.toPeriod());
    }

    /** joda.time.DateTime formatting */
    public static String formatDate(DateTime dateTime) {
        return formatDate(dateTime, I18N.getDateFormat());
    }
    public static String formatDate(DateTime dateTime, String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(dateTime);
    }

    public static String formatTime(DateTime dateTime){
        return formatDate(dateTime, "HH:mm");
    }

    public static String formatDateTime(DateTime dateTime) {
        return formatDateTime(dateTime, I18N.getDateFormat()+" HH:mm");
    }
    public static String formatDateTime(DateTime dateTime, String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(dateTime);
    }

    public static String formatDouble(Number number) {
        DecimalFormat fmt = new DecimalFormat("#,##0.00");
        return fmt.format(number);
    }

    public static String format(Number number) {
        DecimalFormat fmt = new DecimalFormat("+#,##0.00;-#");
        return fmt.format(number);
    }


}
