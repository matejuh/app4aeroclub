package utils;

import com.google.gson.*;
import models.User;
import play.Logger;

import java.lang.reflect.Type;

/**
 * User: matej
 * Date: 18.9.12
 */
public class UserSerializer implements JsonSerializer<User>{

    @Override
    public JsonElement serialize(User user, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject obj=new JsonObject();
        obj.addProperty("id",user.id);
        obj.addProperty("label",user.toString());
        obj.addProperty("paymentType",user.paymentType.id);
        return obj;
    }
}
