package utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * User: matej
 * Date: 14.9.12
 */
public class GsonExclusionStrategy implements ExclusionStrategy{
    private final Class<?> typeToExclude;

    public boolean shouldSkipField(FieldAttributes fieldAttributes) {

        return fieldAttributes.getAnnotation(DontSerialize.class) != null;
    }

    public boolean shouldSkipClass(Class<?> aClass) {

        return (this.typeToExclude != null && this.typeToExclude == aClass)
                || aClass.getAnnotation(DontSerialize.class) != null;
    }

    public GsonExclusionStrategy(Class<?> typeToExclude) {

        this.typeToExclude = typeToExclude;
    }
}
