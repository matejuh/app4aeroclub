package utils;

import com.google.gson.*;
import models.Flight;
import models.User;
import models.cashflows.CashFlow;
import models.cashflows.Fee;
import models.cashflows.FlightBill;
import org.joda.time.DateTime;
import play.Logger;

import java.lang.reflect.Type;

/**
 * User: matej
 * Date: 19.9.12
 */
public class CashFlowSerializer implements JsonSerializer<CashFlow>{
    @Override
    public JsonElement serialize(CashFlow cashFlow, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject cashFlowJson=new JsonObject();
        cashFlowJson.add("user",new UserSerializer().serialize(cashFlow.user,CashFlow.class,jsonSerializationContext));
        cashFlowJson.add("date",new DateSerializer().serialize(cashFlow.date,DateTime.class,jsonSerializationContext));
        cashFlowJson.add("amount",new JsonPrimitive(cashFlow.amount));
        cashFlowJson.add("type",new JsonPrimitive(cashFlow.type.toString()));
        cashFlowJson.add("id",new JsonPrimitive(cashFlow.id));

        if(cashFlow.type==CashFlow.FlowType.fee){
            cashFlowJson.add("description",new JsonPrimitive(((Fee)cashFlow).price.name));
        }
        else if(cashFlow.type==CashFlow.FlowType.flight){
            Flight flight=((FlightBill)cashFlow).flight;
            cashFlowJson.add("description",new JsonPrimitive(CustomExtensions.formatDate(flight.departureTime)+" "+flight.airplane.sign));
        }
        else{
            cashFlowJson.add("description",new JsonPrimitive(cashFlow.description));
        }
        return cashFlowJson;
    }
}
