package utils;

import net.sf.oval.configuration.annotation.Constraint;
import play.data.validation.InPastCheck;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * User: matej
 * Date: 15.8.12
 */


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(checkWith = DateTimeInPastCheck.class)
public @interface DateTimeInPast {

    String message() default DateTimeInPastCheck.mes;
    String value() default "";
}

