package utils;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;
import org.joda.time.DateTime;
import play.exceptions.UnexpectedException;
import play.utils.Utils;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * User: matej
 * Date: 20.10.12
 */
public class DateTimeInFutureCheck extends AbstractAnnotationCheck<DateTimeInFuture>{
    final static String mes = "validation.datetime.future";
    DateTime reference;

    @Override
    public void configure(DateTimeInFuture future) {
        try {
            this.reference = future.value().equals("") ? new DateTime() : new DateTime(Utils.AlternativeDateFormat.getDefaultFormatter().parse(future.value()));
        } catch (ParseException ex) {
            throw new UnexpectedException("Cannot parse date " + future.value(), ex);
        }
        if (!future.value().equals("") && future.message().equals(mes)) {
            setMessage("validation.datetime.after");
        } else {
            setMessage(future.message());
        }
    }

    @Override
    public boolean isSatisfied(Object validatedObject, Object value, OValContext context, Validator validator) throws OValException {
//        Logger.info("date time validator");
        requireMessageVariablesRecreation();
        if (value == null) {
            return true;
        }
        if (value instanceof DateTime) {
            try {
                return reference.isBefore((DateTime) value);
            } catch (Exception e) {
                return false;
            }
        }
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Map<String, String> createMessageVariables() {
        Map<String, String> messageVariables = new HashMap<String, String>();
        messageVariables.put("reference", reference.toString("dd.MM.yy"));
        return messageVariables;
    }
}
