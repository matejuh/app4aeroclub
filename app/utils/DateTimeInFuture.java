package utils;

import net.sf.oval.configuration.annotation.Constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * User: matej
 * Date: 20.10.12
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(checkWith = DateTimeInFutureCheck.class)
public @interface DateTimeInFuture {
    String message() default DateTimeInFutureCheck.mes;
    String value() default "";
}
