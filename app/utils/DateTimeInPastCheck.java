package utils;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;
import org.joda.time.DateTime;
import play.Logger;
import play.data.validation.InPast;
import play.exceptions.UnexpectedException;
import play.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * User: matej
 * Date: 15.8.12
 */
public class DateTimeInPastCheck extends AbstractAnnotationCheck<DateTimeInPast> {

    final static String mes = "validation.datetime.past";
    DateTime reference;

    @Override
    public void configure(DateTimeInPast past) {
        try {
            this.reference = past.value().equals("") ? new DateTime() : new DateTime(Utils.AlternativeDateFormat.getDefaultFormatter().parse(past.value()));
        } catch (ParseException ex) {
            throw new UnexpectedException("Cannot parse date " + past.value(), ex);
        }
        if (!past.value().equals("") && past.message().equals(mes)) {
            setMessage("validation.datetime.before");
        } else {
            setMessage(past.message());
        }
    }

    @Override
    public boolean isSatisfied(Object validatedObject, Object value, OValContext context, Validator validator) throws OValException {
//        Logger.info("date time validator");
        requireMessageVariablesRecreation();
        if (value == null) {
            return true;
        }
        if (value instanceof DateTime) {
            try {
                return reference.isAfter((DateTime) value);
            } catch (Exception e) {
                return false;
            }
        }
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Map<String, String> createMessageVariables() {
        Map<String, String> messageVariables = new HashMap<String, String>();
        messageVariables.put("reference", reference.toString("dd.MM.yy"));
        return messageVariables;
    }
}
