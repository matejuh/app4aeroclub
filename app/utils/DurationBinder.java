package utils;

import org.joda.time.Duration;
import play.Logger;
import play.data.binding.Global;
import play.data.binding.TypeBinder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * User: matej
 * Date: 14.8.12
 *
 * Class for binding joda.time.Duration from fixture file in format HH:MM
 */

@Global
public class DurationBinder implements TypeBinder<Duration>{

    @Override
    public Object bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType) throws Exception {
        String[] values = value.split(":");
        return new Duration((long)Integer.parseInt(values[0])*3600000+Integer.parseInt(values[1])*60000);
    }
}
