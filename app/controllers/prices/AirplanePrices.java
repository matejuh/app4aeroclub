package controllers.prices;

import models.Airplane;
import models.PaymentType;
import models.prices.AirplanePayment;
import models.prices.AirplanePrice;
import models.prices.AirplanePricePrice;
import org.joda.time.DateTime;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import utils.AirplanePriceConditionComparator;
import utils.CustomExtensions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: matej
 * Date: 27.9.12
 */
public class AirplanePrices extends Controller {
    @Before(unless = "deleteAirplanePrice")
    private static void loadPaymentTypes() {
        renderArgs.put("paymentTypes", PaymentType.findAllActive());
    }

    @Before(only = {"indexAirplanePrices","adminAirplanePrice"})
    private static void loadAirplanes() {
        renderArgs.put("airplanes", Airplane.findAll());
    }
    public static void indexAirplanePrices(){
        render("Prices/admin/adminAirplanePrices.html");
    }

    public static void adminAirplanePrice(Long airplaneId, Long id){
        final AirplanePayment airplanePayment;

        Airplane airplane=Airplane.findById(airplaneId);
        notFoundIfNull(airplane,"Airplane with id "+airplaneId+" not found. Can't show airplane price history.");

        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't admin airplane price history with this payment type");

        airplanePayment=AirplanePayment.find("byAirplaneAndPaymentType",airplane,paymentType).first();

        render("Prices/admin/adminAirplanePrices.html",airplane,paymentType,airplanePayment);
    }

    public static void addAirplanePrice(Long airplaneId){
        final AirplanePayment airplanePayment;
        final AirplanePrice airplanePrice;

        Airplane airplane=Airplane.findById(airplaneId);
        notFoundIfNull(airplane,"Airplane with id "+airplaneId+" not found. Can't add prices.");

        airplanePayment=new AirplanePayment(airplane);
        airplanePrice=new AirplanePrice();

        render("Prices/forms/formAirplanePrices.html",airplanePayment,airplanePrice);
    }

    public static void addAirplanePricePrice(Long airplaneId, Long id){
        AirplanePayment airplanePayment;
        final AirplanePrice airplanePrice;

        Airplane airplane=Airplane.findById(airplaneId);
        notFoundIfNull(airplane,"Airplane with id "+airplaneId+" not found. Can't add prices.");
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't add airplane prices with this payment type");

        airplanePayment=AirplanePayment.find("byAirplaneAndPaymentType",airplane,paymentType).first();
        if(airplanePayment==null){
            airplanePayment=new AirplanePayment(airplane,paymentType);
        }
        airplanePrice=new AirplanePrice();

        render("Prices/forms/formAirplanePrices.html",airplanePayment,airplanePrice);
    }

    public static void editAirplanePrice(Long airplaneId, Long id, Long priceId){
        final AirplanePayment airplanePayment;
        final AirplanePrice airplanePrice;

        Airplane airplane=Airplane.findById(airplaneId);
        notFoundIfNull(airplane,"Airplane with id "+airplaneId+" not found. Can't add prices.");
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't add airplane prices with this payment type");

        airplanePayment=AirplanePayment.find("byAirplaneAndPaymentType",airplane,paymentType).first();
        notFoundIfNull(airplanePayment,airplane.sign+" "+paymentType+" price not found. Can't edit.");

        airplanePrice= AirplanePrice.findById(priceId);
        notFoundIfNull(airplanePrice,"Airplane price id "+priceId+" not found. Can't edit.");

        render("Prices/forms/formAirplanePrices.html",airplanePayment,airplanePrice);
    }

    public static void saveAirplanePrice(AirplanePayment airplanePayment,AirplanePrice airplanePrice){
        Logger.debug("save "+airplanePayment.airplane);
        final AirplanePayment existingAirplanePayment;
        existingAirplanePayment=AirplanePayment.find("byAirplaneAndPaymentType",airplanePayment.airplane,airplanePayment.paymentType).first();
        if(existingAirplanePayment!=null){
            airplanePayment=existingAirplanePayment;
        }
        if(airplanePayment.id!=null){
            airplanePrice.parent=airplanePayment;
        }
        else{
            validation.valid(airplanePayment);
        }

        validation.valid(airplanePrice);

        if(airplanePrice.prices!=null){
            for(AirplanePricePrice price:airplanePrice.prices){
                AirplanePricePrice existing=AirplanePricePrice.find("byPriceAndCond",price.price,price.cond).first();
                if(existing!=null){
                    price=existing;
                    validation.valid(price);
                }
            }
        }

        if(validation.hasErrors()){
            Logger.debug(validation.errorsMap().toString());
            render("Prices/forms/formAirplanePrices.html",airplanePayment,airplanePrice);
        }

        airplanePrice.parent=airplanePayment;
        Collections.sort(airplanePrice.prices,new AirplanePriceConditionComparator());
        airplanePrice.save();

        flash.success(airplanePayment.airplane.sign+" "+airplanePayment.paymentType+" price valid since "+CustomExtensions.formatDate(airplanePrice.date)+" saved successfully.");
        adminAirplanePrice(airplanePayment.airplane.id,airplanePayment.paymentType.id);
    }


    public static void deleteAirplanePrice(Long airplaneId, Long id, Long priceId){
        final AirplanePayment airplanePayment;
        final AirplanePrice airplanePrice;

        Airplane airplane=Airplane.findById(airplaneId);
        notFoundIfNull(airplane,"Airplane with id "+airplaneId+" not found. Can't delete it's price.");
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't delete airplane prices with this payment type");

        airplanePayment=AirplanePayment.find("byAirplaneAndPaymentType",airplane,paymentType).first();
        notFoundIfNull(airplanePayment,airplane.sign+" "+paymentType+" price not found. Can't delete.");

        airplanePrice= AirplanePrice.findById(priceId);
        notFoundIfNull(airplanePrice,"Airplane price "+priceId+" not found. Can't edit.");

        if(airplanePrice.date.isAfterNow()){
            airplanePrice.delete();
            flash.success(airplane.sign+ " price valid since "+CustomExtensions.formatDate(airplanePrice.date)+" successfully deleted.");
        }
        else{
            flash.error("Can't delete price in the past");
        }

        adminAirplanePrice(airplaneId,id);

    }
}
