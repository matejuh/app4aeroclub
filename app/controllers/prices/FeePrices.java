package controllers.prices;

import models.cashflows.Fee;
import models.prices.FeePrice;
import models.prices.JobPrice;
import models.prices.Price;
import org.joda.time.DateTime;
import play.Logger;
import play.data.validation.*;
import play.data.validation.Error;
import play.mvc.Before;
import play.mvc.Controller;
import utils.CustomExtensions;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

/**
 * User: matej
 * Date: 25.9.12
 */
public class FeePrices extends Controller{
    @Before(unless = "deleteFeePrice")
    private static void loadFeePrices() {
        renderArgs.put("feePrices", FeePrice.findAll());
    }

    public static void indexFeePrices(){
        List<FeePrice> feePrices= FeePrice.findAll();
        render("Prices/admin/adminFeePrices.html");
    }

    public static void adminFeePrice(Long id){
        FeePrice feePrice=FeePrice.findById(id);
        notFoundIfNull(feePrice, "Fee price with id " + id + " not found. Can't admin.");
        render("Prices/admin/adminFeePrices.html", feePrice);
    }

    public static void addFeePrice(){
        final FeePrice feePrice;
        final Price price;

        feePrice=new FeePrice();
        price=new Price();

        render("Prices/forms/formFeePrice.html",feePrice,price);
    }

    public static void addFeePricePrice(Long id){
        final FeePrice feePrice;
        final Price price;

        feePrice=FeePrice.findById(id);
        notFoundIfNull(feePrice,"Fee price with id "+id+" not found. Can't edit.");
        price=new Price();

        render("Prices/forms/formFeePrice.html",feePrice,price);
    }

    public static void editFeePrice(Long id, Long priceId){
        final FeePrice feePrice;
        final Price price;

        feePrice=FeePrice.findById(id);
        notFoundIfNull(feePrice,"Fee price with id "+id+" not found. Can't edit.");

        price=Price.findById(priceId);
        notFoundIfNull(price,"Fee price with id "+priceId+" not found. Can't edit.");

        render("Prices/forms/formFeePrice.html",feePrice,price);
    }

    public static void saveFeePrice(@Valid FeePrice feePrice, Price price){
        Logger.debug("saving..");
        final FeePrice existingFeePrice;
        existingFeePrice=FeePrice.find("byName",feePrice.name).first();
        if(existingFeePrice!=null){
            feePrice=existingFeePrice;
        }
        if(feePrice.id!=null){
            price.parent=feePrice;
        }
        validation.valid(price);

        if(validation.hasErrors()){
            render("Prices/forms/formFeePrice.html",feePrice,price);
        }
        price.parent=feePrice;
        price.save();

        flash.success("Fee " + feePrice.name + " valid since " + CustomExtensions.formatDate(price.date) + " has been succesfully saved.");

        adminFeePrice(feePrice.id);
    }

    public static void deleteFeePrice(Long id,Long priceId){
        FeePrice feePrice=FeePrice.findById(id);
        notFoundIfNull(feePrice,"Fee price with id "+id+" not found. Can't delete");

        Price price=Price.findById(priceId);
        notFoundIfNull(price,"Price with id "+priceId+" not found. Can't delete.");

        if(price.date.isAfterNow()){
            price.delete();
            flash.success(feePrice.name+" fee, valid since "+CustomExtensions.formatDate(price.date)+" successfully deleted.");
        }
        else{
            flash.error("Can't delete price in the past.");
        }
        adminFeePrice(feePrice.id);
    }
}
