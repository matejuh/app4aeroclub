package controllers.prices;

import models.PaymentType;
import models.prices.Price;
import models.prices.WinchPrice;
import play.Logger;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import utils.CustomExtensions;

/**
 * User: matej
 * Date: 25.9.12
 */
public class WinchPrices extends Controller{

    @Before(unless = "deleteWinchPrice")
    private static void loadPaymentTypes() {
        renderArgs.put("paymentTypes", PaymentType.findAllActive());
    }

    public static void indexWinchPrices(){
        render("Prices/admin/adminWinchPrices.html");
    }

    public static void adminWinchPrice(Long id){
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't admin winch price with this payment type.");
        WinchPrice winchPrice=WinchPrice.find("byPaymentType",paymentType).first();
        render("Prices/admin/adminWinchPrices.html",winchPrice,paymentType);
    }

    public static void addWinchPrice(){
        final WinchPrice winchPrice;
        final Price price;

        winchPrice=new WinchPrice();
        price=new Price();

        render("Prices/forms/formWinchPrice.html",winchPrice,price);
    }

    public static void addWinchPricePrice(Long id){
        WinchPrice winchPrice;
        final Price price;

        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't add any winch price with this payment type.");
        winchPrice=WinchPrice.find("byPaymentType",paymentType).first();

        if(winchPrice==null){
            winchPrice=new WinchPrice(paymentType);
        }

        price=new Price();

        render("Prices/forms/formWinchPrice.html",winchPrice,price);
    }

    public static void editWinchPrice(Long id, Long priceId){
        final WinchPrice winchPrice;
        final Price price;

        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't edit any winch price with this payment type.");

        winchPrice=WinchPrice.find("byPaymentType",paymentType).first();
        notFoundIfNull(winchPrice,"Winch price with payment type "+paymentType.name+" not found. Can't edit.");

        price=Price.findById(priceId);
        notFoundIfNull(price,"Price with id "+priceId+" does not exist, can't edit.");

        render("Prices/forms/formWinchPrice.html",winchPrice,price);
    }
    public static void saveWinchPrice(@Valid WinchPrice winchPrice,Price price){
        final WinchPrice existingWinchPrice;
        existingWinchPrice=WinchPrice.find("byPaymentType",winchPrice.paymentType).first();
        if(existingWinchPrice!=null){
            winchPrice=existingWinchPrice;
        }
        if(winchPrice.id!=null){
            price.parent=winchPrice;
        }
        validation.valid(price);

        if(validation.hasErrors()){
            Logger.debug(validation.errorsMap().toString());
            render("Prices/forms/formWinchPrice.html", winchPrice, price);
        }
        price.parent=winchPrice;
        price.save();

        flash.success("Winch "+winchPrice.paymentType.name+" price valid since "+CustomExtensions.formatDate(price.date)+" successfully saved.");
        adminWinchPrice(winchPrice.paymentType.id);
    }

    public static void deleteWinchPrice(Long id,Long priceId){
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't delete any winch price with this payment type.");

        WinchPrice winchPrice=WinchPrice.find("byPaymentType",paymentType).first();
        notFoundIfNull(winchPrice,"Winch price with payment type "+paymentType.name+" not found. Can't delete.");

        Price price= Price.findById(priceId);
        notFoundIfNull(price,"Price with id "+priceId+" does not exist. Can't be deleted.");
        if(price.date.isAfterNow()){
            price.delete();
            flash.success("Winch "+winchPrice.paymentType.name+" price valid since "+ CustomExtensions.formatDate(price.date)+" successfully deleted.");
        }
        else{
            flash.error("Can't delete price in the past");
        }
        adminWinchPrice(winchPrice.paymentType.id);
    }
}
