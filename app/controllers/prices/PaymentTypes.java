package controllers.prices;

import models.AutocompleteValue;
import models.PaymentType;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * User: matej
 * Date: 25.9.12
 */
public class PaymentTypes extends Controller {

    @Before(only={"adminPaymentTypes","editPaymentType","savePaymentType"})
    private static void loadPaymentTypes() {
        renderArgs.put("paymentTypes", PaymentType.findAll());
    }

    public static void adminPaymentTypes(){
        render("Prices/admin/adminPaymentTypes.html");
    }

    public static void editPaymentType(Long id){
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id " + id + " not found!");
        render("Prices/admin/adminPaymentTypes.html", paymentType);
    }

    public static void savePaymentType(@Valid PaymentType paymentType){
        if(validation.hasErrors()) {
            render("Prices/admin/adminPaymentTypes.html",paymentType);
        }
        paymentType.save();
        flash.success("Payment type " + paymentType.name + " has been succesfully saved.");
        adminPaymentTypes();
    }

    public static void disablePaymentType(Long id){
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id " + id + " not found! Could not be deleted.");
        paymentType.isActive=false;
        paymentType.save();
        flash.success("Payment type " + paymentType.name + " has been succesfully disabled.");
        adminPaymentTypes();
    }

    public static void enablePaymentType(Long id){
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id " + id + " not found! Could not be deleted.");
        paymentType.isActive=true;
        paymentType.save();
        flash.success("Payment type " + paymentType.name + " has been succesfully enabled.");
        adminPaymentTypes();
    }

    public static int AUTOCOMPLETE_MAX = 10;

    public static void autocomplete(final String term) {
        List<PaymentType>inactivePaymentTypes=PaymentType.find("lower(firstName) like lower(?) and isActive=?","%"+term+"%",false).fetch(AUTOCOMPLETE_MAX);
        final List<AutocompleteValue> response = new ArrayList<AutocompleteValue>();
        Iterator<PaymentType> it=inactivePaymentTypes.iterator();
        while(it.hasNext()){
            PaymentType inactivePaymentType=it.next();
            response.add(new AutocompleteValue(inactivePaymentType.id, inactivePaymentType.name));
        }
        renderJSON(response);
    }
}
