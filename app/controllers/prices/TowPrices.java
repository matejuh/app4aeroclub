package controllers.prices;

import models.Aircraft;
import models.PaymentType;
import models.prices.TowPrice;
import models.prices.TowPricePrice;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import utils.CustomExtensions;

/**
 * User: matej
 * Date: 28.9.12
 */
public class TowPrices extends Controller {
    @Before(unless = "deleteTowPrice")
    private static void loadPaymentTypes() {
        renderArgs.put("paymentTypes", PaymentType.findAllActive());
    }

    @Before(only = {"indexTowPrices","adminTowPrice"})
    private static void loadTowPlane() {
        renderArgs.put("towPlanes",Aircraft.find("byIsTowPlane",true).fetch());
    }

    public static void indexTowPrices(){
        render("Prices/admin/adminTowPrices.html");
    }

    public static void adminTowPrice(Long towPlaneId, Long id){
        Aircraft towPlane=Aircraft.find("byIdAndIsTowPlane",towPlaneId,true).first();
        notFoundIfNull(towPlane,"Tow plane with id "+towPlaneId+" not found. Can't show tow prices history.");

        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't admin tow price with this payment type");

        TowPrice towPrice=TowPrice.find("byTowPlaneAndPaymentType",towPlane,paymentType).first();

        render("Prices/admin/adminTowPrices.html",towPlane,paymentType,towPrice);
    }

    public static void addTowPrice(Long towPlaneId){
        final TowPrice towPrice;
        final TowPricePrice price;

        Aircraft towPlane=Aircraft.find("byIdAndIsTowPlane",towPlaneId,true).first();
        notFoundIfNull(towPlane,"Tow plane with id "+towPlane+" not found. Can't add tow price.");

        towPrice=new TowPrice(towPlane);
        price=new TowPricePrice();

        render("Prices/forms/formTowPrice.html",towPrice,price);
    }
    public static void addTowPricePrice(Long towPlaneId,Long id){
        TowPrice towPrice;
        final TowPricePrice price;

        Aircraft towPlane=Aircraft.find("byIdAndIsTowPlane",towPlaneId,true).first();
        notFoundIfNull(towPlane,"Tow plane with id "+towPlane+" not found. Can't add tow price.");

        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't add tow price with this payment type.");

        towPrice=TowPrice.find("byTowPlaneAndPaymentType",towPlane,paymentType).first();
        if(towPrice==null){
            towPrice=new TowPrice(towPlane,paymentType);
        }

        price=new TowPricePrice();

        render("Prices/forms/formTowPrice.html",towPrice,price);
    }
    public static void editTowPrice(Long towPlaneId,Long id,Long priceId){
        Aircraft towPlane=Aircraft.find("byIdAndIsTowPlane", towPlaneId, true).first();
        notFoundIfNull(towPlane,"Tow plane with id "+towPlane+" not found. Can't edit it's tow price.");

        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't edit price with this payment type.");

        TowPrice towPrice=TowPrice.find("byTowPlaneAndPaymentType",towPlane,paymentType).first();
        notFoundIfNull(towPrice,towPlane+" tow "+paymentType+" price not found. Can't edit.");

        TowPricePrice price=TowPricePrice.findById(priceId);
        notFoundIfNull(price,"Price with id "+id+" not found. Can't edit.");

        render("Prices/forms/formTowPrice.html",towPrice,price);
    }

    public static void saveTowPrice(@Valid TowPrice towPrice, TowPricePrice price){
        final TowPrice existingTowPrice;
        existingTowPrice=TowPrice.find("byTowPlaneAndPaymentType",towPrice.towPlane,towPrice.paymentType).first();
        if(existingTowPrice!=null){
            towPrice=existingTowPrice;
        }
        if(towPrice.id!=null){
            price.parent=towPrice;
        }
        validation.valid(price);

        if(validation.hasErrors()){
            render("Prices/forms/formTowPrice.html",towPrice,price);
        }
        price.parent=towPrice;
        price.save();

        flash.success(towPrice.towPlane+" tow "+towPrice.paymentType+" price valid since "+CustomExtensions.formatDate(price.date)+" successfully saved.");
        adminTowPrice(towPrice.towPlane.id,towPrice.paymentType.id);
    }


    public static void deleteTowPrice(Long towPlaneId,Long id,Long priceId){
        Aircraft towPlane=Aircraft.find("byIdAndIsTowPlane",towPlaneId,true).first();
        notFoundIfNull(towPlane,"Tow plane with id "+towPlane+" not found. Can't delete it's tow price.");

        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't delete price with this payment type.");

        TowPrice towPrice=TowPrice.find("byTowPlaneAndPaymentType",towPlane,paymentType).first();
        notFoundIfNull(towPrice,towPlane+" tow "+paymentType+" price not found. Can't delete.");

        TowPricePrice price=TowPricePrice.findById(priceId);
        notFoundIfNull(price,"Price with id "+id+" not found. Can't delete.");

        if(price.date.isAfterNow()){
            price.delete();
            flash.success(towPrice.towPlane+" tow "+towPrice.paymentType+" price valid since "+CustomExtensions.formatDate(price.date)+" successfully deleted.");
        }
        else{
            flash.error("Can't delete price in the past");
        }

        adminTowPrice(towPrice.towPlane.id,towPrice.paymentType.id);
    }

}
