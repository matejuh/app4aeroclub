package controllers.prices;

import models.prices.JobPrice;
import models.prices.Price;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import utils.CustomExtensions;

/**
 * User: matej
 * Date: 25.9.12
 */
public class JobPrices extends Controller{
    @Before(unless = "deleteJobPrice")
    private static void loadJobPrices() {
        renderArgs.put("jobPrices", JobPrice.findAll());
    }


    public static void indexJobPrices(){
        render("Prices/admin/adminJobPrices.html");
    }

    public static void adminJobPrice(Long id){
        JobPrice jobPrice=JobPrice.findById(id);
        notFoundIfNull(jobPrice,"Job price with id "+id+" not found. Can't admin.");
        render("Prices/admin/adminJobPrices.html",jobPrice);
    }

    public static void addJobPrice(){
        final JobPrice jobPrice;
        final Price price;

        jobPrice=new JobPrice();
        price=new Price();

        render("Prices/forms/formJobPrice.html",jobPrice,price);
    }

    public static void addJobPricePrice(Long id){
        final JobPrice jobPrice;
        final Price price;

        jobPrice=JobPrice.findById(id);
        notFoundIfNull(jobPrice,"Job price with id "+id+" not found. Can't edit.");
        price=new Price();

        render("Prices/forms/formJobPrice.html",jobPrice,price);
    }

    public static void editJobPrice(Long id, Long priceId){
        final JobPrice jobPrice;
        final Price price;

        jobPrice=JobPrice.findById(id);
        notFoundIfNull(jobPrice,"Job price with id "+id+" not found. Can't edit.");

        price=Price.findById(priceId);
        notFoundIfNull(price,"Job price with id "+id+" not found. Can't edit.");

        render("Prices/forms/formJobPrice.html",jobPrice,price);
    }

    public static void saveJobPrice(@Valid JobPrice jobPrice,Price price){
        final JobPrice existingJobPrice;
        existingJobPrice=JobPrice.find("byJobType",jobPrice.jobType).first();
        if(existingJobPrice!=null){
            jobPrice=existingJobPrice;
        }
        if(jobPrice.id!=null){
            price.parent=jobPrice;
        }

        if(validation.hasErrors()){
            render("Prices/forms/formJobPrice.html", jobPrice, price);
        }
        price.parent=jobPrice;
        price.save();

        flash.success("Job "+jobPrice.jobType+" price valid since "+CustomExtensions.formatDate(price.date)+" successfully saved.");
        adminJobPrice(jobPrice.id);
    }

    public static void deleteJobPrice(Long id,Long priceId){
        JobPrice jobPrice=JobPrice.findById(id);
        notFoundIfNull(jobPrice,"Job price with id "+id+" not found. Can't delete.");
        Price price=Price.findById(priceId);
        notFoundIfNull(price,"Price id with id "+id+" not found. Can't delete.");
        if(price.date.isAfterNow()){
            price.delete();
            flash.success(jobPrice.jobType+" job price, valid since "+CustomExtensions.formatDate(price.date)+" successfully deleted.");
        }
        else{
            flash.error("Can't delete price in the past.");
        }
        adminJobPrice(jobPrice.id);
    }

}

