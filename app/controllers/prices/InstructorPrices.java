package controllers.prices;

import models.PaymentType;
import models.prices.InstructorPrice;
import models.prices.InstructorPricePrice;
import play.Logger;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import utils.CustomExtensions;

/**
 * User: matej
 * Date: 25.9.12
 */
public class InstructorPrices extends Controller {
    @Before(unless ="deleteInstructorPrice")
    private static void loadPaymentTypes() {
        renderArgs.put("paymentTypes", PaymentType.findAllActive());
    }
    public static void indexInstructorPrices(){
        render("Prices/admin/adminInstructorPrices.html");
    }
    public static void adminInstructorPrice(Long id){
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't admin this payment type price.");
        InstructorPrice aircraftInstructorPrice=InstructorPrice.find("byFlightTypeAndPaymentType", InstructorPrice.FlightType.aircraft,paymentType).first();
        InstructorPrice gliderInstructorPrice= InstructorPrice.find("byFlightTypeAndPaymentType", InstructorPrice.FlightType.glider,paymentType).first();
        render("Prices/admin/adminInstructorPrices.html",aircraftInstructorPrice,gliderInstructorPrice,paymentType);
    }
    public static void addInstructorPrice(InstructorPrice.FlightType flightType){
        final InstructorPrice instructorPrice;
        final InstructorPricePrice price;

        instructorPrice=new InstructorPrice();
        instructorPrice.flightType=flightType;

        price=new InstructorPricePrice();
        render("Prices/forms/formInstructorPrice.html", instructorPrice, price);
    }

    public static void addInstructorPricePrice(InstructorPrice.FlightType flightType,Long id){
        InstructorPrice instructorPrice;
        final InstructorPricePrice price;

        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't add this payment type instructor price.");
        instructorPrice= InstructorPrice.find("byFlightTypeAndPaymentType",flightType,paymentType).first();
        if(instructorPrice==null){
            instructorPrice=new InstructorPrice(flightType,paymentType);
        }

        price=new InstructorPricePrice();

        render("Prices/forms/formInstructorPrice.html", instructorPrice, price);
    }

    public static void editInstructorPrice(InstructorPrice.FlightType flightType,Long id, Long priceId){
        final InstructorPrice instructorPrice;
        final InstructorPricePrice price;

        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't add this payment type instructor price.");

        instructorPrice= InstructorPrice.find("byFlightTypeAndPaymentType",flightType,paymentType).first();
        notFoundIfNull(instructorPrice,CustomExtensions.capFirst(flightType.toString())+" instructor price with payment type "+paymentType+" not found. Can't edit.");

        price= InstructorPricePrice.findById(priceId);
        notFoundIfNull(price,CustomExtensions.capFirst(flightType.toString())+" instructor price with payment type "+paymentType+" and "+priceId+" not found. Can't edit.");

        render("Prices/forms/formInstructorPrice.html", instructorPrice, price);
    }


    public static void saveInstructorPrice(@Valid InstructorPrice instructorPrice,InstructorPricePrice price){
        final InstructorPrice existingInstructorPrice;
        existingInstructorPrice= InstructorPrice.find("byFlightTypeAndPaymentType", instructorPrice.flightType, instructorPrice.paymentType).first();
        if(existingInstructorPrice!=null){
            instructorPrice=existingInstructorPrice;
        }
        if(instructorPrice.id!=null){
            price.parent=instructorPrice;
        }
        validation.valid(price);
        if(validation.hasErrors()){
            render("Prices/forms/formInstructorPrice.html",instructorPrice,price);
        }
        price.parent=instructorPrice;
        price.save();

        flash.success(instructorPrice.flightType+" instructor "+instructorPrice.paymentType.name+" price valid since "+CustomExtensions.formatDate(price.date)+" successfully saved.");
        adminInstructorPrice(instructorPrice.paymentType.id);
    }

    public static void deleteInstructorPrice(InstructorPrice.FlightType flightType,Long id,Long priceId){
        PaymentType paymentType=PaymentType.findById(id);
        notFoundIfNull(paymentType,"Payment type with id "+id+" not found. Can't delete any price with this payment type.");

        InstructorPrice instructorPrice= InstructorPrice.find("byFlightTypeAndPaymentType",flightType,paymentType).first();
        notFoundIfNull(instructorPrice,CustomExtensions.capFirst(flightType.toString())+" instructor price with payment type "+paymentType.name+" not found. Can't delete.");
        InstructorPricePrice price= InstructorPricePrice.findById(priceId);
        notFoundIfNull(price,"Price with id "+priceId+" not found. Can't be deleted.");
        if(price.date.isAfterNow()){
            price.delete();
            flash.success("Instructor price, payment type "+instructorPrice.paymentType.name+", valid since "+ CustomExtensions.formatDate(price.date)+" successfully deleted.");
        }
        else{
            flash.error("Can't delete price in the past.");
        }
        adminInstructorPrice(instructorPrice.paymentType.id);
    }

}
