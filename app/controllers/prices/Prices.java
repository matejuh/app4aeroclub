package controllers.prices;

import models.*;
import models.prices.*;
import play.Logger;
import play.mvc.Controller;

import java.util.List;

/**
 * User: matej
 * Date: 4.8.12
 */
public class Prices extends Controller{

    public static void indexPrices(){
        indexOtherPrices();
    }

    public static void indexOtherPrices(){
        List<JobPrice>jobPrices=JobPrice.findAll();
        List<FeePrice>feePrices=FeePrice.findAll();
        List<InstructorPrice>aircraftInstructorPrices=InstructorPrice.find("byFlightType",InstructorPrice.FlightType.aircraft).fetch();
        List<InstructorPrice>gliderInstructorPrices=InstructorPrice.find("byFlightType",InstructorPrice.FlightType.glider).fetch();
        List<WinchPrice>winchPrices=WinchPrice.findAll();
        render("Prices/list/indexOtherPrices.html", jobPrices, feePrices, aircraftInstructorPrices, gliderInstructorPrices, winchPrices);
    }

    public static void indexAircraftPrices(){
        List<Aircraft>aircraftList=Aircraft.findAll();
        render("Prices/list/indexAircraftPrices.html",aircraftList);
    }

    public static void indexGliderPrices(){
        List<Glider>gliderList=Glider.findAll();
        render("Prices/list/indexGliderPrices.html",gliderList);
    }

    public static void showAircraftPrices(Long id){
        Aircraft airplane=Aircraft.findById(id);
        notFoundIfNull(airplane,"Aircraft with id "+id+" not found, cant show aircraft's prices");
        List<AirplanePrice>prices=airplane.getActualPrices();
        List<TowPricePrice>towPrices=null;
        if(airplane.isTowPlane){
            towPrices=airplane.getActualTowPrices();
        }
        render("Prices/list/showAircraftPrices.html",airplane,prices,towPrices);
    }

    public static void showGliderPrices(Long id){
        Glider airplane=Glider.findById(id);
        notFoundIfNull(airplane,"Glider with id "+id+" not found, cant show glider's prices");
        List<AirplanePrice>prices=airplane.getActualPrices();
        render("Prices/list/showGliderPrices.html",airplane,prices);
    }
}
