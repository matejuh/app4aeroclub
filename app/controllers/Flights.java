package controllers;

import models.*;
import models.cashflows.CashFlow;
import models.cashflows.FlightBill;
import org.joda.time.DateTime;
import play.Logger;
import play.data.validation.*;
import play.db.jpa.JPA;
import play.mvc.Before;
import play.mvc.Controller;
import controllers.payment.CustomPayment;
import controllers.payment.DefaultPayment;
import controllers.payment.PaymentStrategy;
import play.mvc.results.NotFound;
import utils.CustomExtensions;
import utils.PdfBuilder;

import javax.persistence.NoResultException;
import javax.persistence.Parameter;
import javax.persistence.Query;
import java.io.File;
import java.util.*;

/**
 * User: matej
 * Date: 30.6.12
 */
public class Flights extends Controller{

    public static long results=0;
    private static boolean filter=true;

    @Before(only = {"addAircraftFlight","addAircraftsFlight","editAircraftFlight","addUsersAircraftFlight","saveAircraftFlight","addGliderFlight","addGlidersFlight","editGliderFlight","addUsersGliderFlight","saveGliderFlight"})
    public static void loadPaymentTypes(){
        renderArgs.put("paymentTypes", PaymentType.findAllActive());
    }

    @Before(only = {"addAircraftFlight","addAircraftsFlight","editAircraftFlight","addUsersAircraftFlight","saveAircraftFlight"})
    public static void loadLastAircraftFlight(){
        renderArgs.put("lastAdded",AircraftFlight.find("order by id desc").first());
    }

    @Before(only = {"addGliderFlight","addGlidersFlight","editGliderFlight","addUsersGliderFlight","saveGliderFlight"})
    public static void loadLastGliderFlight(){
        renderArgs.put("lastAdded",GliderFlight.find("order by id desc").first());
    }

    public static void indexFlights(){
       render();
    }

    public static void indexAircraftFlights(Long id){
        Aircraft aircraft=Aircraft.findById(id);
        notFoundIfNull(aircraft,"Aircraft id "+id+" not found. Can't show it's flights.");
        render("@indexFlights",aircraft);
    }

    public static void indexFlight(Long id){
        AircraftFlight aircraftFlight=AircraftFlight.findById(id);
        if(aircraftFlight!=null){
            render("@indexAircraftFlight",aircraftFlight);
        }
        GliderFlight gliderFlight=GliderFlight.findById(id);
        if(gliderFlight!=null){
            render("@indexGliderFlight",gliderFlight);
        }
        notFoundIfNull(gliderFlight,"Flight with "+id+" not found. Can't show details.");
    }

    public static void indexAircraftFlight(Long id){
        AircraftFlight aircraftFlight=AircraftFlight.findById(id);
        notFoundIfNull(aircraftFlight,"Aircraft flight with id "+id+" not found. Can't show details.");
        render("@indexAircraftFlight",aircraftFlight);
    }

    public static void indexGliderFlight(Long id){
        GliderFlight gliderFlight=GliderFlight.findById(id);
        notFoundIfNull(gliderFlight,"Glider flight with id "+id+" not found. Can't show details.");
        render("@indexGliderFlight",gliderFlight);
    }


    public static void indexGliderFlights(Long id){
        Glider glider=Glider.findById(id);
        notFoundIfNull(glider,"Glider id "+id+" not found. Can't show it's flights.");
        render("@indexFlights",glider);
    }

    public static void indexUserFlights(Long id){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+" not found. Can't show user's flights.");
        render("@indexFlights",user);
    }

    public static void getFlights(String format, DateTime from, DateTime to, String sign, String user){
        List<Flight>flights=filterFlights(null, null, from, to, sign, user, "asc");
        PdfBuilder builder=new PdfBuilder(flights,"flights");
        File pdf=builder.createFlightsPdf(filter);
        renderBinary(pdf,pdf.getName());
    }

    public static void listFlights(Integer size, Integer page, DateTime from, DateTime to, String sign, String user,String sDate){
        page = (page != null) ? page : 1;
        List<Flight> flights=filterFlights(size, page, from, to, sign, user, sDate);
        long pages=results;
        render("Flights/listFlights.html",flights, size, page, pages, sDate,filter);
    }

    private static List<Flight> filterFlights(Integer size, Integer page, DateTime from, DateTime to, String sign, String user,String sDate){
        List<Flight>flights=null;
        page = page != null ? page : 1;
        long pages=0;

        String query="from Flight flight join flight.pilot as pilot left join flight.secondCrewMember as secondCrewMember join flight.airplane as airplane";
        List<Object>params=new ArrayList<Object>();

        try{
            Long id=Long.parseLong(user);
            query+=query.contains("where") ? " and " : " where ";
            query+="pilot.id=? or secondCrewMember.id=?";
            params.add(id);
            params.add(id);
        }
        catch (NumberFormatException ex){
            if(user.trim()!=null && !user.trim().isEmpty()){
                query+=query.contains("where") ? " and " : " where ";
                query+="lower(pilot.lastName) like ? or lower(secondCrewMember.lastName) like ?";
                params.add(("%"+user+"%").toLowerCase());
                params.add(("%"+user+"%").toLowerCase());
            }
        }

        if(from!=null && to!=null){
            query+=query.contains("where") ? " and " : " where ";
            query+="flight.departureTime between ? and ?";
            params.add(from);
            params.add(to);
        }

        if(sign.trim()!=null && !sign.trim().isEmpty()){
            query+=query.contains("where") ? " and " : " where ";
            query+="lower(airplane.sign) like ?";
            params.add(("%"+sign+"%").toLowerCase());
        }

//        Logger.debug(query);
        if(size!=null){
            results=Flight.count("select count(*) "+query,params.toArray());
            results=CustomExtensions.page(results,size);
        }

        if(!sDate.isEmpty()){
            query+=(" order by flight.departureTime "+sDate);
        }

        //for default sorting
        if(!query.contains("order by")){
            query+=" order by flight.departureTime desc";
        }

        filter = query.contains("where");

        if(page!=null && size!=null){
            flights= Flight.find("select distinct flight "+query, params.toArray()).fetch(page, size);
        }
        else{
            flights= Flight.find("select distinct flight "+query, params.toArray()).fetch();
        }
        return flights;

    }

    public static void editFlight(Long id){
        Flight flight=AircraftFlight.findById(id);
        if(flight==null){
            flight=GliderFlight.findById(id);
        }
        else{
            redirect("/flight/aircraft/"+id+"/edit");
        }
        notFoundIfNull(flight,"Flight with id "+id+" not found. Can't edit.");
        redirect("/flight/glider/"+id+"/edit");
    }

    public static void deleteFlight(Long id){
        AircraftFlight aircraftFlight=AircraftFlight.findById(id);
        if(aircraftFlight!=null){
            aircraftFlight.delete();
        }
        else{
            GliderFlight gliderFlight=GliderFlight.findById(id);
            notFoundIfNull(gliderFlight,"Flight id "+id+" not found. Can't delete.");
            gliderFlight.delete();
        }
        flash.success("Flight "+id+" successfully deleted.");
        indexFlights();
    }

    public static void addAircraftFlight(){
        final AircraftFlight flight;
        flight=new AircraftFlight();
        render("Flights/forms/formAircraftFlight.html",flight);
    }

    public static void addAircraftsFlight(Long id){
        Aircraft airplane=Aircraft.findById(id);
        notFoundIfNull(airplane,"Aircraft id "+id+"not found. Can't add it's flight.");
        final AircraftFlight flight;
        flight=new AircraftFlight();
        render("Flights/forms/formAircraftFlight.html",flight,airplane);
    }

    public static void editAircraftFlight(Long id){
        final AircraftFlight flight;

        flight=AircraftFlight.findById(id);
        notFoundIfNull(flight,"Aircraft flight with id "+id+" not found. Can't edit.");

        List<FlightBill>billsList;
        if(flight.hasDefaultPayment){
            billsList=null;
        }
        else{
            billsList=flight.billsList;
        }
        GliderFlight towedFlight=flight.towedFlight;
        render("Flights/forms/formAircraftFlight.html",flight,towedFlight,billsList);
    }
    public static void saveAircraftFlight(AircraftFlight flight, GliderFlight towedFlight,List<FlightBill>billsList){
        Logger.debug("aircraft flight " + flight.pilot + " " + billsList + " " + flight.isFlightWithInstructor + " " + flight.departureTime);

        if(billsList!=null){
            flight.hasDefaultPayment=false;
            validateBillsList(billsList);
        }

        if(flight.isTowFlight){
           flight.propagateTowFields(towedFlight);
           validation.valid(towedFlight);
        }
        validation.valid(flight);

        if(validation.hasErrors()){
            Logger.info(validation.errorsMap().toString());
            render("Flights/forms/formAircraftFlight.html",flight,towedFlight,billsList);
        }

        flight.arrivalTime=flight.departureTime.plus(flight.duration);

        flight.save();
        if(flight.isTowFlight){
            towedFlight.save();
        }

        //TODO
        flash.success("success");
        indexFlights();
    }



    private static void validateBillsList(List<FlightBill> billsList) {
        int i=0;
        double sum=0.0;
        for(FlightBill flightBill:billsList){
            if(flightBill.user.id!=null){
                flightBill.user=User.findById(flightBill.user.id);
            }
            else{
                validation.required(flightBill.user.id).key("billsList[" + i + "].user");
            }
            validation.required(flightBill.flightPart).key("billsList[" + i + "].flightPart");
            validation.min(flightBill.flightPart,0.0).key("billsList[" + i + "].flightPart");
            validation.max(flightBill.flightPart,100.0).key("billsList[" + i + "].flightPart");
            i++;
            if(flightBill.flightPart!=null){
                sum+=flightBill.flightPart;
            }
        }
        validation.min("billsList",sum,99.99).message("Sum of flight parts must be 100, but is: "+sum);
        validation.max("billsList", sum, 100.0).message("Sum of flight parts must be 100, but is: " + sum);
    }

    public static void addGliderFlight(){
        final GliderFlight flight;
        flight=new GliderFlight();
        render("Flights/forms/formGliderFlight.html",flight);
    }

    public static void addGlidersFlight(Long id){
        Glider airplane=Glider.findById(id);
        notFoundIfNull(airplane,"Glider id "+id+"not found. Can't add it's flight.");
        final GliderFlight flight;
        flight=new GliderFlight();
        render("Flights/forms/formGliderFlight.html",flight,airplane);
    }
    
    public static void editGliderFlight(Long id){
        final GliderFlight flight;

        flight=GliderFlight.findById(id);
        notFoundIfNull(flight,"Glider flight with id "+id+" not found. Can't edit.");

        List<FlightBill>billsList;
        if(flight.hasDefaultPayment){
            billsList=null;
        }
        else{
            billsList=flight.billsList;
        }

        AircraftFlight towFlight=flight.towFlight;

        render("Flights/forms/formGliderFlight.html",flight,towFlight,billsList);
    }


    public static void saveGliderFlight(GliderFlight flight,AircraftFlight towFlight, List<FlightBill>billsList){
        Logger.info("glider flight "+flight.pilot+" "+billsList);
        final PaymentStrategy paymentStrategy;
        if(billsList!=null){
            flight.hasDefaultPayment=false;
            validateBillsList(billsList);
        }

        if(flight.startType==GliderFlight.StartType.tow){
            flight.towFlight=towFlight;
            flight.propagateTowFields();
            validation.valid(towFlight);
        }
        validation.valid(flight);
        if(validation.hasErrors()){
            Logger.info(validation.errorsMap().toString());
            render("Flights/forms/formFlight.html",flight,towFlight,billsList);
        }

        if(!((Glider)flight.airplane).isDoubleSeater && flight.secondCrewMember!=null){
            flash.error("Two people can't fly in one seater");
        }
        if(flight.isFlightWithInstructor && !((Glider)flight.airplane).isDoubleSeater){
            flash.error("One seater flight can't be flight with instructor");
        }
        if(flight.isFlightWithInstructor && flight.secondCrewMember==null){
            flash.error("Instructor firstName must be specified");

        }
        if(flash.contains("error")){
            render("Flights/forms/formFlight.html",flight,billsList);
        }
        flight.arrivalTime=flight.departureTime.plus(flight.duration);

        flight.save();
        flash.success(flight+" successfully saved.");
        indexFlights();
    }
    public static void addUsersAircraftFlight(Long id){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+"not found. Can't add user's flight.");
        final AircraftFlight flight;
        flight=new AircraftFlight();
        render("Flights/forms/formAircraftFlight.html",flight,user);
    }

    public static void addUsersGliderFlight(Long id){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+"not found. Can't add user's flight.");
        final GliderFlight flight;
        flight=new GliderFlight();
        render("Flights/forms/formGliderFlight.html",flight,user);
    }


    public static void deleteAircraftFlight(Long id){
        AircraftFlight flight=AircraftFlight.findById(id);
        notFoundIfNull(flight,"Aircraft flight with id "+id+" not found. Can't delete");
        flight.delete();
    }

    public static void deleteGliderFlight(Long id){
        GliderFlight flight=GliderFlight.findById(id);
        notFoundIfNull(flight,"Glider flight with id "+id+" not found. Can't delete");
        flight.delete();
    }
}
