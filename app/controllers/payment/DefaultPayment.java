package controllers.payment;

import models.Flight;
import models.cashflows.FlightBill;
import play.Logger;

import java.util.List;

/**
 * User: matej
 * Date: 22.9.12
 */
public class DefaultPayment extends PaymentStrategy {
    @Override
    protected void createFlightBills(double flightCost) {
        Logger.debug("Default payment strategy");
        this.flight.hasDefaultPayment=true;
        if(this.flight.secondCrewMember==null || this.flight.isFlightWithInstructor){
            FlightBill flightBill=new FlightBill(this.flight,this.flight.pilot,100.0,flightCost);
            flightBill.save();
        }
        else{
            FlightBill flightBillPilot=new FlightBill(this.flight,this.flight.pilot,50.0,flightCost);
            FlightBill flightBillSecondCrewMember=new FlightBill(this.flight,this.flight.secondCrewMember,50.0,flightCost);
            flightBillPilot.save();
            flightBillSecondCrewMember.save();
        }
    }
}
