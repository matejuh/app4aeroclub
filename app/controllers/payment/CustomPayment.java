package controllers.payment;

import models.Flight;
import models.cashflows.CashFlow;
import models.cashflows.FlightBill;
import org.joda.time.DateTime;
import play.Logger;

import java.util.List;

/**
 * User: matej
 * Date: 22.9.12
 */
public class CustomPayment extends PaymentStrategy {
    @Override
    protected void createFlightBills(double flightCost) {
        Logger.debug("Custom payment strategy");
        flight.hasDefaultPayment=false;
        for(FlightBill flightBill:this.billsList){
            flightBill.date=this.flight.departureTime;
            flightBill.type= CashFlow.FlowType.flight;
            flightBill.flight=this.flight;
            flightBill.amount=flightCost*(flightBill.flightPart/100.0);
            flightBill.save();
        }
    }
}
