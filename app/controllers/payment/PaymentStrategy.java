package controllers.payment;

import models.Flight;
import models.cashflows.FlightBill;
import models.cashflows.InstructorWork;
import play.Logger;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * User: matej
 * Date: 22.9.12
 */
public abstract class PaymentStrategy {

    protected Flight flight;
    protected List<FlightBill> billsList;
    /**
     * Main payment controller
     * @param flight to pay
     * @param billsList predefined flight payment by user
     * @throws NoResultException
     */
    public void processPayment(Flight flight,List<FlightBill> billsList) throws NoResultException{
        this.flight=flight;
        this.billsList=billsList;
        /** get airplane cost */
        double airplaneRent=flight.getAirplaneRentCost();
        /** get instructor cost*/
        double instructorCost=flight.getInstructorCost();
        /** get start cost (0 for aircraft flights)*/
        double startCost=flight.getStartCost();
        /** delete old bills (needed when updating flight)*/
        deleteOldBills();
        /** create new bills*/
        createFlightBills(airplaneRent + instructorCost + startCost);
        /** instructor payment*/
        if(flight.isFlightWithInstructor){
            createInstructorIncome(instructorCost);
        }

    }

    private void deleteOldBills() {
        Logger.debug("Deleting old bills flight: "+flight.id);
        if(flight.billsList!=null){
            for(FlightBill flightBill:flight.billsList){
                flightBill.delete();
            }
        }
        if(flight.instructorBill!=null){
            flight.instructorBill.delete();
        }
    }

    protected abstract void createFlightBills(double flightCost);

    protected void createInstructorIncome(double instructorCost) {
        InstructorWork instructorWork=new InstructorWork(this.flight,instructorCost);
        instructorWork.save();
    }


}
