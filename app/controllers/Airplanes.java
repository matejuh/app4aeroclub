package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import models.*;
import models.prices.AirplanePrice;
import models.prices.TowPrice;
import models.prices.TowPricePrice;
import play.Logger;
import play.data.validation.*;
import play.mvc.Before;
import play.mvc.Controller;
import utils.AirplaneSerializer;
import utils.UserSerializer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * User: matej
 * Date: 30.6.12
 */
public class Airplanes extends Controller {
    @Before(only={"formAircraft","formGlider","saveAircraft"})
    private static void beforeAirplane(){
        renderArgs.put("paymentTypes",PaymentType.findAllActive());
    }

    public static void indexAirplanes(){
        List<Aircraft> aircraftList= Aircraft.findAll();
        List<Glider>gliderList=Glider.findAll();
        render(aircraftList,gliderList);
    }

    public static void addAircraft(){
        final Aircraft airplane;
        airplane=new Aircraft();
        render("@formAircraft",airplane);
    }

    public static void editAircraft(Long id){
        final Aircraft airplane;

        airplane=Aircraft.findById(id);
        notFoundIfNull(airplane,"Aircraft with id "+id+" not found. Can't edit.");
        render("@formAircraft",airplane);
    }

    public static void addGlider(){
        final Glider airplane;
        airplane=new Glider();

        render("@formGlider",airplane);
    }

    public static void editGlider(Long id){
        final Glider airplane;

        airplane=Glider.findById(id);
        notFoundIfNull(airplane,"Glider with id "+id+" not found. Can't edit.");
        render("@formGlider",airplane);
    }

    public static void saveAircraft(@Valid Aircraft airplane){
        if(validation.hasErrors()) {
            render("@formAirplane", airplane);
        }
        airplane.save();
        flash.success("Saved successfully.");
        indexAircraft(airplane.id);
    }

    public static void saveGlider(@Valid Glider airplane){
        if(validation.hasErrors()) {
            render("@formAirplane", airplane);
        }
        airplane.save();
        flash.success("Saved successfully.");
        indexGlider(airplane.id);
    }


    public static void showAircraftFlights(Long id) {
        Airplane airplane=Airplane.findById(id);
        List<Flight>flights=Flight.find("byAirplane",airplane).fetch();
        Logger.info("Flights "+flights);
        //TODO render view
    }

    public static void indexAircraft(Long id) {
        Aircraft aircraft=Aircraft.findById(id);
        notFoundIfNull(aircraft,"Aircraft id "+id+" not found. Can't show details.");
        List<AirplanePrice>prices=aircraft.getActualPrices();
        List<TowPricePrice>towPrices=null;
        if(aircraft.isTowPlane){
            towPrices=aircraft.getActualTowPrices();
        }
        render("@indexAircraft",aircraft,prices,towPrices);
    }

    public static void indexGlider(Long id) {
        Glider glider=Glider.findById(id);
        notFoundIfNull(glider,"Glider id "+id+" not found. Can't show details.");
        List<AirplanePrice>prices=glider.getActualPrices();
        render("@indexGlider",glider,prices);
    }

    public static int AUTOCOMPLETE_MAX = 10;

    public static void gliderAutocomplete(final String term) {
        List<Airplane>glidersList=Glider.find("lower(sign) like lower(?) or lower(type) like lower(?)","%"+term+"%","%"+term+"%").fetch(AUTOCOMPLETE_MAX);
        JsonObject json=new JsonObject();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Glider.class, new AirplaneSerializer()).create();
        Gson gson = gsonBuilder.serializeNulls().create();
        String obj=gson.toJson(glidersList);
        renderJSON(obj);
    }

    public static void aircraftAutocomplete(final String term) {
        List<Airplane>aircraftList=Aircraft.find("lower(sign) like lower(?) or lower(type) like lower(?)","%"+term+"%","%"+term+"%").fetch(AUTOCOMPLETE_MAX);

        JsonObject json=new JsonObject();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Aircraft.class, new AirplaneSerializer()).create();
        Gson gson = gsonBuilder.serializeNulls().create();
        String obj=gson.toJson(aircraftList);
        renderJSON(obj);
    }

    public static void towPlanesAutocomplete(final String term) {
        List<Airplane>aircraftList=Aircraft.find("lower(sign) like lower(?) or lower(type) like lower(?) and isTowPlane=?","%"+term+"%","%"+term+"%",true).fetch(AUTOCOMPLETE_MAX);
        JsonObject json=new JsonObject();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Aircraft.class, new AirplaneSerializer()).create();
        Gson gson = gsonBuilder.serializeNulls().create();
        String obj=gson.toJson(aircraftList);
        renderJSON(obj);
    }

}
