package controllers;

import models.cashflows.*;
import models.prices.FeePrice;
import models.User;
import org.joda.time.DateTime;
import play.Logger;
import play.data.validation.*;
import play.mvc.Before;
import play.mvc.Controller;
import utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * User: matej
 * Date: 29.7.12
 */
public class CashFlows extends Controller {

    public static long results=0;

    public static void indexCashFlows(){
        render();
    }

    public static void indexUserCashFlows(Long id){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+" not found. Can't show it's cash flows.");
        render("@indexCashFlows",user);
    }

    public static void getCashFlows(String format,CashFlow.FlowType type, DateTime from, DateTime to, String user){
        List<CashFlow>cashFlows=filterCashFlows(null, null, type, from, to, user, "asc",null,null);
        PdfBuilder builder=new PdfBuilder(cashFlows,"cashflows");
        File pdf=builder.createCashFlowsPdf();
        renderBinary(pdf,pdf.getName());
    }

    public static void listCashFlows(Integer size, Integer page, CashFlow.FlowType type, DateTime from, DateTime to, String user, String sDate, String sUser, String sType){

        page = (page != null) ? page : 1;
        List<CashFlow> cashFlows=filterCashFlows(size,page,type,from,to,user,sDate,sUser,sType);
        long pages=results;
        render(cashFlows, size, page, pages,sDate, sType);
    }

    private static List<CashFlow> filterCashFlows(Integer size, Integer page, CashFlow.FlowType type, DateTime from, DateTime to, String user, String sDate, String sUser, String sType){
        List<CashFlow>cashFlows=null;
        page = page != null ? page : 1;
        long pages=0;

        String query="";
        List<Object>params=new ArrayList<Object>();

        if(type!=null){
            query+="type=? ";
            params.add(type);
        }

        if(from!=null && to!=null){
            query+=query.isEmpty() ? "" : " and ";
            query+="date between ? and ?";
            params.add(from);
            params.add(to);
        }

        try{
            Long id=Long.parseLong(user);
            query+=query.isEmpty() ? "" : " and ";
            query+="user.id=?";
            params.add(id);
        }
        catch (NumberFormatException ex){
            if(user.trim()!=null && !user.trim().isEmpty()){
                query+=query.isEmpty() ? "" : " and ";
                query+="(lower(user.firstName) like ? or lower(user.lastName) like ?)";
                params.add("%"+user+"%".toLowerCase());
                params.add("%"+user+"%".toLowerCase());
            }
        }

        if(size!=null){
            results=CashFlow.count(query,params.toArray());
            results=CustomExtensions.page(results,size);
        }

        if(sDate!=null && !sDate.isEmpty()){
            query+=(" order by date "+sDate);
        }

        if(sUser!=null && !sUser.isEmpty()){
            query+=(" order by user.lastName "+sUser+" order by user.firstName asc order by user.id asc");
        }

        if(sType!=null && !sType.isEmpty()){
            query+=(" order by type "+sType);
        }
        //for default sorting
        if(!query.contains("order by")){
            query+="order by date desc";
        }


        if(page!=null && size!=null){
            cashFlows=CashFlow.find(query,params.toArray()).fetch(page, size);
        }
        else{
            cashFlows=CashFlow.find(query,params.toArray()).fetch();
        }

        return cashFlows;
    }

    public static void addIncome(){
        final Income cashFlow;
        cashFlow=new Income();
        render("@formCashFlow",cashFlow);
    }

    public static void addUsersIncome(Long id){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+" not found. Can't add user's income.");
        final Income cashFlow;
        cashFlow=new Income();
        render("@formCashFlow",cashFlow,user);
    }

    public static void editIncome(Long id){
        final Income cashFlow;

        cashFlow=Income.findById(id);
        notFoundIfNull(cashFlow, "Income with id " + id + " not found. Can't edit.");

        render("@formCashFlow",cashFlow);
    }

    public static void saveIncome(@Valid Income cashFlow){
        if(validation.hasErrors()){
            render("CashFlows/formCashFlow.html",cashFlow);
        }
        cashFlow.save();
        flash.success("Income type:"+cashFlow.type+" user: "+cashFlow.user+" amount: "+cashFlow.amount+" succesfully saved");
        indexCashFlows();
    }

    public static void addExpenditure(){
        final Expenditure cashFlow;
        cashFlow=new Expenditure();
        render("@formCashFlow",cashFlow);
    }

    public static void addUsersExpenditure(Long id){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+" not found. Can't add user's expenditure.");
        final Expenditure cashFlow;
        cashFlow=new Expenditure();
        render("@formCashFlow",cashFlow,user);
    }

    public static void editExpenditure(Long id){
        final Expenditure cashFlow;

        cashFlow=Expenditure.findById(id);
        notFoundIfNull(cashFlow, "Expenditure with id " + id + " not found. Can't edit.");

        render("@formCashFlow",cashFlow);
    }

    public static void saveExpenditure(@Valid Expenditure cashFlow){
        if(validation.hasErrors()){
            render("CashFlows/formCashFlow.html",cashFlow);
        }
        cashFlow.save();
        flash.success("Expenditure type: "+cashFlow.type+" user: "+cashFlow.user+" amount: "+cashFlow.amount+" succesfully saved");
        indexCashFlows();
    }

    public static void deleteCashFlow(Long id){
        CashFlow cashFlow=CashFlow.findById(id);
        notFoundIfNull(cashFlow,"Cash-flow id "+id+" not found. Can't delete.");
        cashFlow.delete();
        flash.success("Cash-flow type: "+cashFlow.type+" user: "+cashFlow.user+" amount: "+cashFlow.amount+" succesfully deleted");
        indexCashFlows();
    }

    public static void formFees(){
        List<FeePrice>feePrices=FeePrice.findAll();
        List<User>users=User.findAll();
        render(users, feePrices);
    }

    @Before(only={"payFee"})
    static void parseParams() {
        Logger.debug("here "+params.get("selectedUsers"));
//        String[] items = params.getAll("selectedUser");
//        for (int i=0;i<items.length;i++) {
//            params.put("items["+i+"].id",items[i]);
//        }
    }

    public static void payFee(List<Long>selectedUsers,Long feePriceId){
        validation.required(feePriceId).message("validation.required.short");
        if(validation.hasErrors()){
            Logger.debug(validation.errorsMap().toString());
            List<FeePrice>feePrices=FeePrice.findAll();
            List<User>users=User.findAll();
            render("CashFlows/formFees.html",feePriceId,selectedUsers,feePrices,users);
        }
        Logger.debug(selectedUsers.size()+"");
        FeePrice feePrice=FeePrice.findById(feePriceId);
        notFoundIfNull(feePrice,"Fee price id "+feePriceId+" not found. Can't pay this fee.");
        int i=0;
        for(Long id:selectedUsers){
            if(id!=null){
                User user=User.findById(id);
                Fee fee=new Fee(user,feePrice);
                fee.save();
            }
            Logger.debug("id: "+id);
        }
        flash.success("Fee "+feePrice.name+" has been successfully paid by selected users.");
//        indexCashFlows();
    }

    public static void editFee(Long id){
        Fee fee=Fee.findById(id);
        notFoundIfNull(fee,"Fee id "+id+" not found. Can't edit.");
        List<FeePrice>feePrices=FeePrice.findAll();
        render(fee,feePrices);
    }

    public static void saveFee(@Valid Fee fee){
        if(validation.hasErrors()){
            List<FeePrice>feePrices=FeePrice.findAll();
            render("CashFlows/editFee.html",fee,feePrices);
        }
        fee.save();
        Logger.info("fee "+fee.user.firstName +" "+fee.amount+" "+fee.price);
        flash.success("Fee "+fee.price.name+" user: "+fee.user+" has been successfully saved.");
        indexCashFlows();
    }

    public static void deleteFee(Long id){
        Fee fee=Fee.findById(id);
        notFoundIfNull(fee,"Fee id "+id+" not found. Can't delete.");
        fee.delete();
        flash.success("Fee "+fee.price.name+" user: "+fee.user+" has been successfully deleted.");
        indexCashFlows();
    }
}
