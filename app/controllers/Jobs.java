package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import models.Flight;
import models.User;
import models.cashflows.*;
import models.prices.JobPrice;
import models.prices.Price;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.data.validation.*;
import play.libs.I18N;
import play.mvc.Before;
import play.mvc.Controller;
import utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: matej
 * Date: 31.7.12
 */
public class Jobs extends Controller {

    public static long results=0;

    @Before(only = {"addJob","addUsersJob","editJob","saveJob"})
    public static void loadJobTypes(){
        renderArgs.put("jobPrices",JobPrice.findAll());
    }

    public static void indexJobs(){
        render();
    }

    public static void indexUserJobs(Long id){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+" not found. Can't show user's jobs.");
        render("@indexJobs",user);
    }

    public static void getJobs(String format, DateTime from, DateTime to, String user){
        List<Job>jobs=filterJobs(null, null, from, to, user, "asc",null);
        PdfBuilder builder=new PdfBuilder(jobs,"jobs");
        File pdf=builder.createJobsPdf();
        renderBinary(pdf,pdf.getName());
    }

    public static void listJobs(Integer size, Integer page, DateTime from, DateTime to, String user, String sDate, String sUser){
        page = (page != null) ? page : 1;
        List<Job> jobs=filterJobs(size, page, from, to, user, sDate, sUser);
        long pages=results;
        render(jobs, size, page, pages,sDate,sUser);
    }

    private static List<Job> filterJobs(Integer size, Integer page, DateTime from, DateTime to, String user, String sDate, String sUser){
        List<Job>jobs=null;
        page = page != null ? page : 1;
        long pages=0;

        String query="";
        List<Object>params=new ArrayList<Object>();

        if(from!=null && to!=null){
            query+="date between ? and ?";
            params.add(from);
            params.add(to);
        }

        try{
            Long id=Long.parseLong(user);
            query+=query.isEmpty() ? "" : " and ";
            query+="user.id=?";
            params.add(id);
        }
        catch (NumberFormatException ex){
            if(user.trim()!=null && !user.trim().isEmpty()){
                query+=query.isEmpty() ? "" : " and ";
                query+="(lower(user.firstName) like ? or lower(user.lastName) like ?)";
                params.add("%"+user+"%".toLowerCase());
                params.add("%"+user+"%".toLowerCase());
            }
        }

        if(size!=null){
            results= Job.count(query,params.toArray());
            results=CustomExtensions.page(results,size);
        }

        if(sDate!=null && !sDate.isEmpty()){
            query+=(" order by date "+sDate);
        }

        if(sUser!=null && !sUser.isEmpty()){
            query+=(" order by user.lastName "+sUser+" order by user.firstName asc order by user.id asc");
        }

        //for default sorting
        if(!query.contains("order by")){
            query+="order by date desc";
        }

        if(page!=null && size!=null){
            jobs=Job.find(query,params.toArray()).fetch(page, size);
        }
        else{
            jobs=Job.find(query,params.toArray()).fetch();
        }

        return jobs;
    }

    public static void addJob(){
        Job job=new Job();
        render("@formJob",job);
    }

    public static void addUsersJob(Long id){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+" not found. Can't add user's job.");
        Job job=new Job();
        render("@formJob",job,user);
    }

    public static void editJob(Long id){
        final Job job;
        job= Job.findById(id);
        notFoundIfNull(job,"Job id "+id+" not found. Can't edit.");

        render("@formJob",job);
    }

    public static void saveJob(@Valid Job job){
        validation.required(job.description).key("job.description");
        if(validation.hasErrors()){
            render("@formJob",job);
        }
        Price price=job.price.getPrice(job.date);
        job.amount=CashFlow.getCost(job.duration,price.price);
        job.save();
        flash.success("Job user: "+job.user+" date: "+CustomExtensions.formatDate(job.date)+" description: "+job.description+" has been successfully saved.");
        indexJobs();
    }

    public static void deleteJob(Long id){
        Job job= Job.findById(id);
        notFoundIfNull(job,"Job id "+id+" not found. Can't delete.");
        job.delete();
        flash.success("Job user: "+job.user+" date: "+CustomExtensions.formatDate(job.date)+" description: "+job.description+" has been sucsessfully deleted.");
        indexJobs();
    }
}


