package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import models.AutocompleteValue;
import models.Licence;
import models.PaymentType;
import models.User;
import models.cashflows.CashFlow;
import org.joda.time.DateTime;
import play.Logger;
import play.data.validation.Valid;
import play.db.jpa.JPA;
import play.mvc.Before;
import play.mvc.Catch;
import play.mvc.Controller;
import utils.CustomExtensions;
import utils.PaginationInfo;
import utils.PdfBuilder;
import utils.UserSerializer;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * User: matej
 * Date: 30.6.12
 */
public class Users extends Controller {

    public static long results=0;

    @Before(only = {"addUser","editUser","saveUser"})
    public static void loadPaymentTypes(){
        renderArgs.put("paymentTypes", PaymentType.findAllActive());
    }

    @Catch(Exception.class)
    public static void catchAllExceptions(Throwable exception){
        Logger.error(exception,"Users controller exception: "+exception);
        Logger.error(exception,exception.getStackTrace().toString());
        error(exception.getMessage());
    }

    public static void indexUsers(){
        render();
    }

    public static void indexUser(Long id) {
        User user=User.findById(id);
        notFoundIfNull(user,"User with id " + id + " not found!");
        render(user);
    }

    public static void addUser(){
        final User user=new User();
        render("@formUser",user);
    }

    public static void editUser(Long id){
        final User user;
        user=User.findById(id);
        notFoundIfNull(user, "User with id " + id + " not found! Can't edit.");
        render("@formUser",user);
    }

    public static void saveUser(@Valid User user){
        if(validation.hasErrors()) {
            render("@formUser",user);
        }
        user.save();
        flash.success("User successfully saved.");
        indexUser(user.id);
    }

    public static int AUTOCOMPLETE_MAX = 10;

    public static void autocomplete(String term) {
        term=term.trim();
        List<User>users=null;

        String [] terms=term.split(" ");
        Long id;
        try{
            id=Long.parseLong(terms[0]);
        }
        catch (NumberFormatException ex){
             id=null;
        }
        switch (terms.length){
            case 1:
                users=User.find("id = ? or lower(firstName) like lower(?) or lower(lastName) like lower(?)",id,"%"+terms[0]+"%","%"+terms[0]+"%").fetch(AUTOCOMPLETE_MAX);
                break;
            case 2:
                users=User.find("id = ? and lower(firstName) like lower(?) or lower(firstName) like lower(?) and lower(lastName) like lower(?)",id,"%"+terms[1]+"%","%"+terms[0]+"%","%"+terms[1]+"%").fetch(AUTOCOMPLETE_MAX);
                break;
            case 3:
                Logger.debug(id+" "+terms[1]+" "+terms[2]);
                users=User.find("id=? and lower(firstName) like lower(?) and lower(lastName) like lower(?)",id,"%"+terms[1]+"%","%"+terms[2]+"%").fetch(AUTOCOMPLETE_MAX);
                break;
            default:
                break;
        }

        JsonObject json=new JsonObject();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(User.class, new UserSerializer()).create();
        Gson gson = gsonBuilder.serializeNulls().create();
        String obj=gson.toJson(users);
        renderJSON(obj);
    }

    public static void getBalances(String action, String format, Integer size, Integer page, String user, Double balance, String comparator, String monthYear, String sId, String sUser, String sBalance){
        List<User> users=filterUsers(null,null,user,balance,comparator,monthYear,sId,sUser,sBalance);
        PdfBuilder builder=new PdfBuilder(users,action);
        File pdf=builder.createBalancesPdf(monthYear);
        renderBinary(pdf,pdf.getName());
    }

    public static void getContacts(String action, String format, Integer size, Integer page, String user, Double balance, String comparator, String monthYear, String sId, String sUser, String sBalance){
        List<User> users=filterUsers(null,null,user,balance,comparator,monthYear,sId,sUser,sBalance);
        PdfBuilder builder=new PdfBuilder(users,action);
        File pdf=builder.createContactsPdf();
        renderBinary(pdf,pdf.getName());
    }

    public static void listUsers(String action, String format, Integer size, Integer page, String user, Double balance, String comparator, String monthYear, String sId, String sUser, String sBalance){
        page = (page != null) ? page : 1;
        List<User> userList=filterUsers(size,page,user,balance,comparator,monthYear,sId,sUser,sBalance);
        long pages=results;
        render(userList, size, page, pages, sId,sUser,sBalance);
    }

    public static List<User> filterUsers(Integer size, Integer page, String user, Double balance, String comparator, String monthYear, String sId, String sUser, String sBalance){
        List<User> userList= null;

        String query="";
        List<Object>params=new ArrayList<Object>();

        if(balance!=null){
            query+="actualBalance.balance "+comparator+" ?";
            params.add(balance);
        }

        try{
            Long id=Long.parseLong(user);
            query+=query.isEmpty() ? "" : " and ";
            query+="id=?";
            params.add(id);
        }
        catch (NumberFormatException ex){
            if(user.trim()!=null && !user.trim().isEmpty()){
                query+=query.isEmpty() ? "" : " and ";
                query+="(lower(firstName) like ? or lower(lastName) like ?)";
                params.add("%"+user+"%".toLowerCase());
                params.add("%"+user+"%".toLowerCase());
            }
        }

        if(size!=null){
            results=User.count(query,params.toArray());
            results=CustomExtensions.page(results,size);
        }


        if(!sBalance.isEmpty()){
            query+=(" order by actualBalance.balance "+sBalance);
        }

        if(!sUser.isEmpty()){
            query+=(" order by lastName "+sUser+" order by firstName asc");
        }

        if(!sId.isEmpty()){
            query+=(" order by id "+sId);
        }

        //for default sorting
        if(!query.contains("order by")){
            query+=(" order by lastName asc order by firstName asc");
        }
//        Logger.debug("page: "+page+", size: "+size);
        if(page!=null && size!=null){
            userList= User.find(query, params.toArray()).fetch(page, size);
        }
        else{
            userList= User.find(query, params.toArray()).fetch();
        }
        if(monthYear!=null && !monthYear.isEmpty()){
            int month=Integer.parseInt(monthYear.trim().split("/")[0]);
            int year=Integer.parseInt(monthYear.trim().split("/")[1]);
            for(User u:userList){
                u.balance=u.getBalance(year,month);
            }
        }
        else{
            for(User u:userList){
                u.balance=u.actualBalance.balance;
            }
        }

        return userList;
    }


    public static void disableUser(Long id){
        //TODO
    }

    public static void enableUser(Long id){
        //TODO
    }

    public static void activateUser(Long id){
        //TODO
    }

    public static void deactivateUser(Long id){
        //TODO
    }

    public static void addLicence(Long id){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+" not found. Can't add his licence");
        Licence licence=new Licence();
        licence.user=user;
        render("@formLicence",licence);
    }

    public static void editLicence(Long id, Long licenceId){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+" not found. Can't edit his licence");
        Licence licence=Licence.find("byIdAndUser",licenceId,user).first();
        notFoundIfNull(licence,user+" licence id "+licenceId+" not found. Can't edit.");
        render("@formLicence",licence,user);
    }

    public static void saveLicence(@Valid Licence licence){
        Logger.debug(licence.user.toString());
        if(validation.hasErrors()) {
            render("@formLicence",licence);
        }
        licence.save();
        flash.success(licence.user+"'s "+licence.type+" licence valid until: "+CustomExtensions.formatDate(licence.validity)+" successfully saved.");
        indexUser(licence.user.id);
    }

    public static void deleteLicence(Long id, Long licenceId){
        User user=User.findById(id);
        notFoundIfNull(user,"User id "+id+" not found. Can't delete his licence");
        Licence licence=Licence.find("byIdAndUser",licenceId,user).first();
        notFoundIfNull(licence,user+" licence id "+id+" not found. Can't delete.");
        licence.delete();
        flash.success(user+" licence id "+licenceId+" successfully deleted.");
        indexUser(user.id);
    }

}
