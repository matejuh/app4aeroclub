function setArrival(){
      var departureDate=new Date($('#departure').datetimepicker('getDate'));
      var durationTime=$('#duration').val().split(":");
      var hours=parseInt(durationTime[0]);
      var minutes=parseInt(durationTime[1]);
      arrivalDate=departureDate;
      arrivalDate.setHours(departureDate.getHours()+hours);
      arrivalDate.setMinutes(departureDate.getMinutes()+minutes);
      //set end date field
      $('#arrival').val($.datepicker.formatDate('dd.mm.yy', arrivalDate)+" "+('0' + arrivalDate.getHours()).slice(-2)+":"+('0' + arrivalDate.getMinutes()).slice(-2));
}

/** Init times in flight form*/
$(document).ready(function(){
    //flight start
    $('#departure').datetimepicker({
        dateFormat: 'dd.mm.yy',
        timeFormat: 'hh:mm',
        firstDay: 1,
        maxDate: new Date(),
        showOn: "button",
        buttonImage: "/public/img/glyphicons_045_calendar.png",
        buttonImageOnly: true,
        addSliderAccess: true,
        sliderAccessArgs: { touchonly: false },
        onClose: function() {
            if($('#duration').val()!=''){
                setArrival();
            }
        },
    });

    //$('#arrival').datetimepicker();

    //duration timepicker
    $('#duration').timepicker({
        timeOnly: true,
        showOn: 'button',
        buttonImage: '/public/img/glyphicons_055_stopwatch.png',
        buttonImageOnly: true,
        addSliderAccess: true,
        sliderAccessArgs: { touchonly: false },
        onClose: function (){
            if ($('#departure').val() != '') {
                setArrival();
            }
        }
    });

    $('#departure').next('.ui-datepicker-trigger').click(function(){
        if($("#departure").val()!=''){
           $("#departure").datetimepicker("setDate",$("#departure").val());
        }
        else if($("#departure").attr("last")!=''){
           $("#departure").datetimepicker("setTime",$("#departure").attr("last"));
        }
        else{
        }

    });

    $('#duration').next('.ui-datepicker-trigger').click(function(){
        if($('#duration').val()!=""){
            var date=new Date($('#departure').datetimepicker('getDate'));
            var durationTime=$("#duration").val().split(":");
            var hours=parseInt(durationTime[0]);
            var minutes=parseInt(durationTime[1]);
            date.setHours(hours);
            date.setMinutes(minutes);
            $("#duration").timepicker("setTime",$("#duration").val());

        }
        else if($("#duration").attr("last")!=""){
             var date=new Date($('#departure').datetimepicker('getDate'));
             var durationTime=$("#duration").attr("last").split(":");
             var hours=parseInt(durationTime[0]);
             var minutes=parseInt(durationTime[1]);
             date.setHours(hours);
             date.setMinutes(minutes);
             $("#duration").datetimepicker("setDate",date);
        }
        else{
        }

    });

    $('#departure').blur(function(){
        if($('#duration').val()!=''){
            setArrival();
        }
    });

    $('#duration').blur(function(){
        if ($('#departure').val() != '') {
            setArrival();
        }
    });

});

