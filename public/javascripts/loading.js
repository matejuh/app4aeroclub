$(document).ready(function(){
    var spinnerOpts = {
        lines: 15, // The number of lines to draw
        length: 30, // The length of each line
        width: 5, // The line thickness
        radius: 36, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        color: '#000', // #rgb or #rrggbb
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 10
    };

    $.fn.spin = function(spinnerOpts) {
        this.each(function() {
            var $this = $(this),
            spinner = $this.data('spinner');

            if (spinner) spinner.stop();
            if (opts !== false) {
                opts = $.extend({color: $this.css('color')}, spinnerOpts);
                spinner = new Spinner(spinnerOpts).spin(this);
                $this.data('spinner', spinner);
            }
        });
        return this;
    };

    /* add ajax spinner */
    $('body').ajaxStart(function () {
        var target = $("div.dynamicContent").first().get(0)
        var spinner = new Spinner(spinnerOpts).spin(target);
    })
//    .ajaxStop(function () {
//        $("#spinner").remove();
//    });
});
