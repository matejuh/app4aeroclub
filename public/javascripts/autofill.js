/** Autofill functionality (need to add last and lastId property for input elements)*/
$(document).ready(function(){
    $.fn.focusNextInputField = function() {
        return this.each(function() {
        var fields = $(this).parents('form:eq(0),body').find(':input').not('[type=hidden]').not('[tabindex=-1]');
        var index = fields.index( this );
        if ( index > -1 && ( index + 1 ) < fields.length ) {
            fields.eq( index + 1 ).focus();
        }
        return false;
        });
    };

    $(':input[type=text]:not(.autocomplete)').bind('keyup.f4',function(){
        var last=$(this).attr('last');
        $(this).val(last);
        $(this).focusNextInputField();
    });
    $(':input.autocomplete').bind('keyup.f4',function(){
        var last=$(this).attr('last');
        var lastId=$(this).attr('lastId');
        $(this).attr('value',last);
        $(this).next('input[type=hidden]').attr('value',lastId);
        $(this).focusNextInputField();
    });
    $(':radio').bind('keyup.f4',function(){
        var last=$(this).attr('last');
        var val=$(this).attr('value');
        if(val==last){
            $(this).attr('checked', true);
        }
        else{
            $(this).attr('checked', false);
        }
        $(this).focusNextInputField();
    });
    $(':checkbox').bind('keyup.f4',function(){
        var last=$(this).attr('last');
        if(last=='true'){
            $(this).attr('checked', true);
        }
        else{
            $(this).attr('checked', false);
        }
        $(this).focusNextInputField();
    });

});