/** Autocomplete */
function addAutocomplete(element){
    var $input = element;
    // Create a hidden input with the same form control name to submit the value.
    var $hidden = $input.next('input:hidden');

    // Set-up the autocomplete widget.
    var serverUrl = $input.data('url');
    element.autocomplete({
        source: serverUrl,
        focus: function(event, ui) {
            // Set the text input value to the focused item's label, instead of the value.
            $input.val(ui.item.label);
            return false;
        },
        select: function(event, ui) {
            // Save the selection item and value in the two inputs.
            $input.val(ui.item.label);
            $hidden.val(ui.item.id);
            if($hidden.attr('name')=="flight.pilot.id"){
                $('input:radio.payment[value="'+ui.item.paymentType+'"]').attr('checked', true);
            }
            return false;
        }
    });
};

$(document).ready(function(){
    $('input.autocomplete').each( function(){
        addAutocomplete($(this));
        $('input.autocomplete').keypress(function (event){
            $(this).next('input:hidden').val('');
        });
    });

    $('form').submit(function() {
        $(this).find('input.autocomplete').attr('disabled', 'disabled');
    });


});