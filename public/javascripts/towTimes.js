function setTowArrival(){
    if ($('#departure').val() != '') {
        var departureDate=new Date($('#departure').datetimepicker('getDate'));
        var durationTime=new Date($('#towDuration').datetimepicker('getDate'));
        var hours=durationTime.getHours();
        var minutes=durationTime.getMinutes();
        towArrivalDate=departureDate;
        towArrivalDate.setHours(departureDate.getHours()+hours);
        towArrivalDate.setMinutes(departureDate.getMinutes()+minutes);
        //set end date field
        $('#towArrival').val($.datepicker.formatDate('dd.mm.yy', towArrivalDate)+" "+('0' + towArrivalDate.getHours()).slice(-2)+":"+('0' + towArrivalDate.getMinutes()).slice(-2));
    }
}

function setUpTimes(){
    $('#towDuration').timepicker({
        showOn: 'button',
        buttonImage: '/public/img/glyphicons_055_stopwatch.png',
        buttonImageOnly: true,
        addSliderAccess: true,
        sliderAccessArgs: { touchonly: false },
        //when changed, the tow end date must be changed also
        onClose: function (){
            setTowArrival();
        }
    });
     console.log("ffff");
    $('#towDuration').on("focus blur",function(){
        console.log("aaa");
        if ($('#towDuration').val() != '') {
            setTowArrival();
        }
    });
}



