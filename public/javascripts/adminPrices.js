$(document).ready(function(){
    $("a.deletePrice").click(function(event) {
        event.preventDefault();
        var targetUrl = $(this).attr("href");
        $('#confirmationDeletePrice').modal();
        $("#deletePrice").attr("href",targetUrl);
    });

    $('#date').datepicker({
        dateFormat: 'dd.mm.yy',
        showOn: "button",
        buttonImage: "/public/img/glyphicons_045_calendar.png",
        buttonImageOnly: true,
        minDate: "+1d",
        firstDay: 1
    });
})