$(document).ready(function () {
    $('.date').datepicker({
        dateFormat: 'dd.mm.yy',
        showOn: "button",
        buttonImage: "/public/img/glyphicons_045_calendar.png",
        buttonImageOnly: true,
        minDate: "+1d",
        firstDay: 1,
        changeMonth: true,
        changeYear: true
    });
});